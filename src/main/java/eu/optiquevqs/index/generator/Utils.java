package eu.optiquevqs.index.generator;


// Misc java
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.*;
import java.util.*;



import org.apache.jena.query.ResultSet;
import org.apache.jena.query.*;


// This util class contains many useful functions which may be used by many classes
public class Utils {

    // Return the uri of rdf:type
    public static String getRdfTypeString() {
        return "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
    }

    // Generic method to print a result set.
    public static void printResultSet(ResultSet resultSet) {
        List<String> s = resultSet.getResultVars();
        System.out.println(s);

        int maxRows = 100;

        while(resultSet.hasNext()){
            maxRows -= 1;
            if (maxRows < 0) break;

            QuerySolution binding = resultSet.nextSolution();                     
            String lineString = "";
            for (String variableName : s) {
                lineString += binding.get(variableName) + "\t";
            }
            System.out.println(lineString);
        }
    }

    // Read a file into a list of strings. One list item per line.
    public static List<String> readListFromFile(String filePath) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            String str;
            List<String> returnList = new ArrayList<String>();
            while ((str = br.readLine()) != null) {
                if (str.length() != 0) returnList.add(str);
            }
            br.close();
            return returnList;
        }
        catch(Exception e) {
        }
        return null;
    }

    // Take a list of strings and write them to a file. One line per list item
    public static void writeListToFile(List<String> l, String outputFileName) {
        try {
            FileWriter fw = new FileWriter(outputFileName);
            for (String item : l) {
                fw.append(item + "\n");
            }
            fw.close();
        }
        catch (Exception e){System.out.println("[ERROR] Failed while writing list to file." + e);}
    }

    // Get the inverse property label.
    public static String getInverseProperty(String propertyLabel) {
        if (propertyLabel.endsWith("^")) {
            return propertyLabel.substring(0, propertyLabel.length() - 1);
        }
        else return  propertyLabel + "^";

    }


    // Give a result set and a variable name. Get the value of the variable in the first result instance in rs
    // this value must be cast to an int
    public static Integer getFirstIntRowByVariableName(ResultSet rs, String variableName) {
        while(rs.hasNext()){
            QuerySolution binding = rs.nextSolution();
            return binding.get(variableName).asLiteral().getInt();
        }
        return null;
    }

    // Give a result set and a variable name. Get the value of the variable in the first result instance in rs
    // this value must be cast to a double.
    public static Double getFirstDoubleRowByVariableName(ResultSet rs, String variableName) {
        while(rs.hasNext()){
            QuerySolution binding = rs.nextSolution();
            return binding.get(variableName).asLiteral().getDouble();
        }
        return null;
    }


    // Opens a map tsv file and returns a string to integer map
    // The data must be stored as: key \t value
    public static Map<String, Integer> getMapFromFile(String filePath) {
        List<String> text = Utils.readListFromFile(filePath);
        Map<String, Integer> returnMap = new HashMap<String, Integer>();
        for (String line : text) {
            String[] arr = line.split("\t"); 
            returnMap.put(arr[0], Integer.parseInt(arr[1]));
        }
        return returnMap;
    }

    // Save a map from string to integer into a file.
    // Store one single mapping per line as key \t value
    public static void writeMapToFile(Map<String, Integer> map, String filePath) {
        List <String> u = new ArrayList<String>();
        for (Map.Entry<String,Integer> entry : map.entrySet()) {
            u.add(entry.getKey() + "\t" + entry.getValue());
            Utils.writeListToFile(u, filePath);
        }
    }
}
