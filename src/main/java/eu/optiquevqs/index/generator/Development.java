package eu.optiquevqs.index.generator;

import eu.optiquevqs.index.generator.*;
import eu.optiquevqs.index.generator.core.*;
import eu.optiquevqs.index.generator.Utils;
import eu.optiquevqs.graph.*;
import eu.optiquevqs.graph.navigation.*;
import eu.optiquevqs.graph.query.*;

import java.nio.file.Files;
import java.nio.file.Paths;

import java.io.IOException;
import java.io.FileWriter; 

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.*;

import org.apache.jena.datatypes.*;
import org.apache.jena.query.*;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.*;

import org.jgrapht.alg.DijkstraShortestPath;

import wikidata.assets.*;
import wikidata.assets.queries.*;

// Main developement file
public class Development {

    public static GraphDatasetConnection d = new GraphDatasetConnection("http://localhost:9999/bigdata/sparql"); // Database connection
    public static NavigationGraph navigationGraph = WikidataAssets.getNavigationGraph("wikidata-2"); // Load navigation graph
    // Set the max cost. We try to find a configuration with less cost than this.

    // maps used to store count values. Faster retrieval
    public static Map<String, Integer> mapPF = null;
    public static Map<String, Integer> mapDS = null;
    public static Map<String, Integer> mapDT = null;
    public static Map<String, Integer> mapCF = null;

    public static long startTime;

    public static List<QueryGraph> q05 = new ArrayList<QueryGraph>();
    public static List<QueryGraph> q062 = new ArrayList<QueryGraph>();
    public static List<QueryGraph> q044 = new ArrayList<QueryGraph>();
    public static List<QueryGraph> qOther = new ArrayList<QueryGraph>();

    public static Map<String, Double> pathWeight = new HashMap<String, Double>(); // which pats in query log are used

    public static void main(String[] args) throws Exception {

        // Set max cost
        double maxCost = 1000000;
        startTime = System.nanoTime();


        Configuration config = new Configuration();
        QueryGraph configgraph = new QueryGraph();

        navigationGraph = makeNavigationGraphBidirectional(navigationGraph);

        // Read in queries from all files. Then filter on size and weight
        double sumQueryWeights = 0.0;
        List <String> dates = new ArrayList<String>();
        // Add query files to use
        List<QueryGraph> queries = new ArrayList<QueryGraph>();
        dates.add("2017-06-12_2017-07-09");
        dates.add("2017-07-10_2017-08-06");
        dates.add("2017-08-07_2017-09-03");
        dates.add("2017-12-03_2017-12-30");
        dates.add("2018-01-01_2018-01-28");
        dates.add("2018-01-29_2018-02-25");
        dates.add("2018-02-26_2018-03-25");
        System.out.println("reading and gathering queries...");
        for (String date : dates) {
            String queryPath = "./files/queries/" + date + ".tsv";
            Set<QueryGraph> queriesTemp = QueryGraphFactory.readOptiqueQueries(queryPath, navigationGraph);
            for (QueryGraph qu : queriesTemp) {
                if (qu.isOptiqueCompatible()) {
                    if (qu.getWeight() > 10.0) {  // Set restrictions on query weight
                        if (qu.vertexSet().size() >= 2) { // Set restrictions on query size
                            queries.add(qu);
                            sumQueryWeights += qu.getWeight();
                        }
                    }
                }
            }
        }
        Set<QueryGraph> tmp = new HashSet<QueryGraph>(queries);


        String pathKey = null;
        Double currentPathWeight = null;

        // loop over all queries, and save paths with their total weight
        // This should be replaced by a recursive function.
        for (QueryGraph query : queries) {

            System.out.println();

            Double weight = query.getWeight();
            // pick a root, and make into rooted query.
            for (QueryVariable rootVariable : query.vertexSet()) {
                if (isObjectVariable(rootVariable, navigationGraph)) {  // Can only extend from object variables
                    String rootVariableLabel = rootVariable.getLabel();
                    query.setRoot(rootVariable);
                    RootedQueryGraph rootedQuery = new RootedQueryGraph(query);
                    rootedQuery.redirectEdges();
                    String rootVariableType = rootVariable.getType();

                    Map<String, Boolean> doneThisRound = new HashMap<String, Boolean>();

                    // outgoing edges 1.
                    for (PropertyEdge edge1 : rootedQuery.outgoingEdgesOf(rootVariable)){
                        String edge1Label = edge1.getLabel();
                        QueryVariable target1 = rootedQuery.getEdgeTarget(edge1);
                        String target1Label = target1.getLabel();
                        String target1Type = target1.getType();
                        pathKey = rootVariableType + " " + edge1Label + " " + target1Type;
                        if (!doneThisRound.getOrDefault(pathKey, false)){
                            doneThisRound.put(pathKey, true);
                            currentPathWeight = pathWeight.getOrDefault(pathKey, 0.0);
                            pathWeight.put(pathKey, currentPathWeight+weight);
                        } 

                        if (isObjectVariable(target1, navigationGraph)) {
                            for (PropertyEdge edge2 : rootedQuery.outgoingEdgesOf(target1)){
                                String edge2Label = edge2.getLabel();
                                QueryVariable target2 = rootedQuery.getEdgeTarget(edge2);
                                String target2Label = target2.getLabel();
                                String target2Type = target2.getType();
                                pathKey = rootVariableType + " " + edge1Label + " " + target1Type + " " + edge2Label + " " + target2Type;
                                if (!doneThisRound.getOrDefault(pathKey, false)){
                                    doneThisRound.put(pathKey, true);
                                    currentPathWeight = pathWeight.getOrDefault(pathKey, 0.0);
                                    pathWeight.put(pathKey, currentPathWeight+weight);
                                } 

                                if (isObjectVariable(target2, navigationGraph)) {
                                    for (PropertyEdge edge3 : rootedQuery.outgoingEdgesOf(target2)){
                                        String edge3Label = edge3.getLabel();
                                        QueryVariable target3 = rootedQuery.getEdgeTarget(edge3);
                                        String target3Label = target3.getLabel();
                                        String target3Type = target3.getType();
                                        pathKey = rootVariableType + " " + edge1Label + " " + target1Type + " " + edge2Label + " " + target2Type + " " + edge3Label + " " + target3Type;
                                        if (!doneThisRound.getOrDefault(pathKey, false)){
                                            doneThisRound.put(pathKey, true);
                                            currentPathWeight = pathWeight.getOrDefault(pathKey, 0.0);
                                            pathWeight.put(pathKey, currentPathWeight+weight);
                                        } 

                                        if (isObjectVariable(target3, navigationGraph)) {
                                            for (PropertyEdge edge4 : rootedQuery.outgoingEdgesOf(target3)){
                                                String edge4Label = edge4.getLabel();
                                                QueryVariable target4 = rootedQuery.getEdgeTarget(edge4);
                                                String target4Label = target4.getLabel();
                                                String target4Type = target4.getType();
                                                pathKey = rootVariableType + " " + edge1Label + " " + target1Type + " " + edge2Label + " " + target2Type + " " + edge3Label + " " + target3Type + " " + edge4Label + " " + target4Type;
                                                if (!doneThisRound.getOrDefault(pathKey, false)){
                                                    doneThisRound.put(pathKey, true);
                                                    currentPathWeight = pathWeight.getOrDefault(pathKey, 0.0);
                                                    pathWeight.put(pathKey, currentPathWeight+weight);
                                                } 

                                                if (isObjectVariable(target4, navigationGraph)) {
                                                    for (PropertyEdge edge5 : rootedQuery.outgoingEdgesOf(target4)){
                                                        String edge5Label = edge5.getLabel();
                                                        QueryVariable target5 = rootedQuery.getEdgeTarget(edge5);
                                                        String target5Label = target5.getLabel();
                                                        String target5Type = target5.getType();
                                                        pathKey = rootVariableType + " " + edge1Label + " " + target1Type + " " + edge2Label + " " + target2Type + " " + edge3Label + " " + target3Type + " " + edge4Label + " " + target4Type + " " + edge5Label + " " + target5Type;
                                                        if (!doneThisRound.getOrDefault(pathKey, false)){
                                                            doneThisRound.put(pathKey, true);
                                                            currentPathWeight = pathWeight.getOrDefault(pathKey, 0.0);
                                                            pathWeight.put(pathKey, currentPathWeight+weight);
                                                        } 

                                                        if (isObjectVariable(target5, navigationGraph)) {
                                                            for (PropertyEdge edge6 : rootedQuery.outgoingEdgesOf(target5)){
                                                                String edge6Label = edge6.getLabel();
                                                                QueryVariable target6 = rootedQuery.getEdgeTarget(edge6);
                                                                String target6Label = target6.getLabel();
                                                                String target6Type = target6.getType();
                                                                pathKey = rootVariableType + " " + edge1Label + " " + target1Type + " " + edge2Label + " " + target2Type + " " + edge3Label + " " + target3Type + " " + edge4Label + " " + target4Type + " " + edge5Label + " " + target5Type + " " + edge6Label + " " + target6Type;
                                                                if (!doneThisRound.getOrDefault(pathKey, false)){
                                                                    doneThisRound.put(pathKey, true);
                                                                    currentPathWeight = pathWeight.getOrDefault(pathKey, 0.0);
                                                                    pathWeight.put(pathKey, currentPathWeight+weight);
                                                                } 

                                                                if (isObjectVariable(target6, navigationGraph)) {
                                                                    for (PropertyEdge edge7 : rootedQuery.outgoingEdgesOf(target6)){
                                                                        String edge7Label = edge7.getLabel();
                                                                        QueryVariable target7 = rootedQuery.getEdgeTarget(edge7);
                                                                        String target7Label = target7.getLabel();
                                                                        String target7Type = target7.getType();
                                                                        pathKey = rootVariableType + " " + edge1Label + " " + target1Type + " " + edge2Label + " " + target2Type + " " + edge3Label + " " + target3Type + " " + edge4Label + " " + target4Type + " " + edge5Label + " " + target5Type + " " + edge6Label + " " + target6Type + " " + edge7Label + " " + target7Type;
                                                                        if (!doneThisRound.getOrDefault(pathKey, false)){
                                                                            doneThisRound.put(pathKey, true);
                                                                            currentPathWeight = pathWeight.getOrDefault(pathKey, 0.0);
                                                                            pathWeight.put(pathKey, currentPathWeight+weight);
                                                                        } 

                                                                        if (isObjectVariable(target7, navigationGraph)) {
                                                                            for (PropertyEdge edge8 : rootedQuery.outgoingEdgesOf(target7)){
                                                                                String edge8Label = edge8.getLabel();
                                                                                QueryVariable target8 = rootedQuery.getEdgeTarget(edge8);
                                                                                String target8Label = target8.getLabel();
                                                                                String target8Type = target8.getType();
                                                                                pathKey = rootVariableType + " " + edge1Label + " " + target1Type + " " + edge2Label + " " + target2Type + " " + edge3Label + " " + target3Type + " " + edge4Label + " " + target4Type + " " + edge5Label + " " + target5Type + " " + edge6Label + " " + target6Type + " " + edge7Label + " " + target7Type + " " + edge8Label + " " + target8Type;
                                                                                if (!doneThisRound.getOrDefault(pathKey, false)){
                                                                                    doneThisRound.put(pathKey, true);
                                                                                    currentPathWeight = pathWeight.getOrDefault(pathKey, 0.0);
                                                                                    pathWeight.put(pathKey, currentPathWeight+weight);
                                                                                } 
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        // Sort to use the query with highest weight first
        Collections.sort(queries, new Comparator<QueryGraph>() {
            @Override
            public int compare(QueryGraph q1, QueryGraph q2) {
                return Double.compare(q2.getWeight(), q1.getWeight());
            }
        });
        System.out.println("HOW MANY QUERIES");
        System.out.println(queries.size() + " queries of weight = " +  sumQueryWeights);

        printSizeProfile(queries);
        for (QueryGraph query : queries) {
            System.out.println(query.getWeight() + "\t" + query);
        }

        // Set search method to use
        Map<String, Set<Configuration>> result = analyzeQueriesGreedySearch(queries);

        System.out.println("done");
        Double cost = getConfigMapCostCells(result, false);
        System.out.println("cost = " + cost);

        Double prec = getPrecisionSa(queries, result, false);
        System.out.println("prec = " + prec);
        System.in.read();
    }



    // Return a random configuration from the set of configurations.
    public static String pickRandomKey(Set<String> zs) throws Exception {
        int size = zs.size();
        int item = new Random().nextInt(size); 
        int i = 0;
        for(String obj : zs) {
            if (i == item) return obj;
            i++;
        }
        return null;
    }

    public static List<String> getSmaller(String lis) {
        String[] l = lis.split(" ");
        List<String> splitted = Arrays.asList(l);
        List<String> returnList = new ArrayList<String>();
        String k = splitted.get(0);
        for (int i = 1 ; i < splitted.size() ; i++) {
            k += " " + splitted.get(i);
            if (i%2 == 0) {
                returnList.add(k);
            }
        }
        return returnList;
    }


    ////////////////////////////////////////////////////////////////////////
    // SEARCH methods: MCTS, Best pick, random etc.
    ////////////////////////////////////////////////////////////////////////

    // This search method will analyse the queries. It will preprocess the whole catalog, and add sum of weights for each possible path
    // then it will pick greedy from this structure
    // This is Greedy Query Weight
    public static Map<String, Set<Configuration>> analyzeQueriesGreedySearch(List<QueryGraph> queries) throws Exception {

        List<Map.Entry<String, Double>> arr2 = new ArrayList<Map.Entry<String, Double>>();
        Set<String> all = new HashSet<String>();
        Set<String> added = new HashSet<String>();

        // put all in set
        for (Map.Entry<String, Double> entry : pathWeight.entrySet()) {
            all.add(entry.getKey());
        }

        while(all.size() > 0) {
            String randomKey = pickRandomKey(all);
            System.out.println("picked random : " + randomKey);
            List<String> listOfSmaller = getSmaller(randomKey);
            for (String l : listOfSmaller) {
                if (! added.contains(l))  {
                    added.add(l);
                    Map.Entry<String,Double> entry = new AbstractMap.SimpleEntry<String, Double>(l, 1.0);
                    arr2.add(entry);
                    all.remove(l);
                    break;
                }
            }
        }

        System.out.println("\n");
        System.out.println(arr2);

        Comparator<Map.Entry<String, Double>> myComparator =
        new Comparator<Map.Entry<String, Double>>() {
                    
                    @Override
                    public int compare(
                          Map.Entry<String, Double> e1,
                          Map.Entry<String, Double> e2) {
         
                        Double int1 = -e1.getValue();
                        Double int2 = -e2.getValue();

                        return int1.compareTo(int2);
                    }
        };

        // try again to sort them
        List<Map.Entry<String, Double>> arr = new ArrayList<Map.Entry<String, Double>>();
        for(Map.Entry<String, Double> e: pathWeight.entrySet()) {
            arr.add(e);
        }
        Collections.sort(arr, myComparator);

        System.out.println("len : " + arr.size());

        System.out.print("\n\n\n");
        for(Map.Entry<String, Double> e: pathWeight.entrySet()) {
            System.out.println(e.getKey() + "\t" + e.getValue());
        }

        // print out
        int count = 0;
        for (Map.Entry<String, Double> entry : pathWeight.entrySet()) {
            System.out.println(entry.getKey() + ": \t" + entry.getValue());
            count += 1;
        }
        System.out.println("\n\n\n---------------------------------");
        System.out.println(count);

        // to return
        Map<String, Configuration> configurations = new HashMap<String, Configuration>();

        // Setting up initial configs
        for (NavigationGraphNode v : navigationGraph.vertexSet()) {
            if (v instanceof ClassNode) {
                Configuration smallestConfig = new Configuration();
                QueryVariable root1 = new QueryVariable("v0", v.getLabel());
                smallestConfig.addVertex(root1);
                smallestConfig.setRoot(root1);
                smallestConfig.redirectEdges();
                configurations.put(v.getLabel(), smallestConfig);
            }
        }
        System.out.println(configurations);

        int cc = 0;

        Map<String, Set<Configuration>> returnMap = new HashMap<String, Set<Configuration>>();

        // pick most popular from sorting
        for(Map.Entry<String, Double> e: arr) {
            cc += 1;
            System.out.println("count = " + cc);
            Double maxWeight = 0.0;
            String maxKey = null;
            maxKey = e.getKey();
            System.out.println(e.getKey() + " " +  e.getValue());

            String[] l = maxKey.split(" ");
            List<String> arrOfStr = new ArrayList<String>();
            arrOfStr = Arrays.asList(l);
            int len = arrOfStr.size();
            String rootClass = arrOfStr.get(0);
            Configuration classConfig = configurations.get(rootClass);
            QueryVariable currentVar = classConfig.getRoot();
            String edge = arrOfStr.get(1);
            String targetClass = arrOfStr.get(2);
            System.out.println("find where to add");
            for (int edgePos = 3 ; edgePos < len ; edgePos+=2) {
                for (PropertyEdge outgoingEdge : classConfig.outgoingEdgesOf(currentVar)){
                    // System.out.println("searching for existing edge...");
                    String edgeLabel = outgoingEdge.getLabel();
                    QueryVariable targetVar = classConfig.getEdgeTarget(outgoingEdge);
                    String targetVariableLabel = targetVar.getLabel();
                    String targetClassInConfig = targetVar.getType();
                    if (edgeLabel.equals(edge) && targetClass.equals(targetClassInConfig)) {
                        currentVar = targetVar;
                        edge = arrOfStr.get(edgePos);
                        targetClass = arrOfStr.get(edgePos+1);
                        break;
                    }
                }
            }
            System.out.println("decided what to add:");
            System.out.println(currentVar + " via " + edge + " to " + targetClass);
            QueryVariable newQV = new QueryVariable(classConfig.getFreeVariableName(), targetClass);
            classConfig.addVertex(newQV);
            classConfig.addEdge(currentVar, newQV, new PropertyEdge(edge));
            System.out.println("new version of class config");
            System.out.println(classConfig);
            configurations.put(rootClass,classConfig);

            Map<String, Set<Configuration>> realConfigurationShape = new HashMap<String, Set<Configuration>>();
            for (Map.Entry<String, Configuration> entry : configurations.entrySet()) {
                Set<Configuration> a = new HashSet<Configuration>();
                a.add(entry.getValue());
                realConfigurationShape.put(entry.getKey(), a);
            }

            // where I calculate cost and precision and write to file
            Double prec = getPrecisionSa(queries, realConfigurationShape, false);
            System.out.println("Precisoin: " + prec);

            Double cost = getConfigMapCostCells(realConfigurationShape, false);
            System.out.println("cost : " + cost);

            writeStringToFile(cost + "\t" + prec + "\t" + "-" + "\t" + "-" + "\t" + realConfigurationShape + "\n", "results-analyze-queries.tsv");
            returnMap = realConfigurationShape;
        }

        System.out.println("done analyzing");

        return returnMap;
    }



    // todo, generalize to more configs and more classes
    // Do a search. Here we need to work with several copies of a config, and we need to know when to configs are the same, maybe?
    // this method calculates precision and cost of each child config, and picks the one with the best priority. The priority can be precision/cost for example.
    // then it picks the best choice and repeat the whole process.
    public static Configuration searchForConfig1GreedyBestPick(List<QueryGraph> queries, String focusClass, boolean exact, double maxCost) throws Exception {
        Configuration smallestConfig = new Configuration();
        QueryVariable root = new QueryVariable("v1", focusClass);
        smallestConfig.addVertex(root);
        smallestConfig.setRoot(root);
        Double smallestCost = getConfigTreeCostCells(smallestConfig, false);
        System.out.println("estimated cost of smallest config = " + smallestCost);
        Double smallestPrecision = getPrecisionSa(queries, focusClass, false, smallestConfig);
        System.out.println("estimated precision of smallest config = " + smallestPrecision);
        System.in.read();

        // Always pick the child config with best priority
        Configuration currentConfig = smallestConfig;
        while (true) {
            Set<Configuration> childConfigs = getChildrenConfigTreesSimple(currentConfig);

            Double bestPriority = 0.0;
            Configuration bestChild = null;
            for (Configuration childConfig : childConfigs) {
                System.out.println("\n-------------------- checkig child config: " + childConfig);
                Double childCost = getConfigTreeCostCells(childConfig, false);
                System.out.println("    cost: " + childCost);
                if (childCost < maxCost) {
                    Double childPrecision = getPrecisionSa(queries, focusClass, false, childConfig);
                    System.out.println("    prec: " + childPrecision);
                    Double priority = (double)childPrecision/(double)childCost;
                    if (priority > bestPriority) {
                        bestPriority = priority;
                        bestChild = childConfig;
                    }
                }
            }

            System.out.println("\n-----------------------------------");
            Double childCost = getConfigTreeCostCells(bestChild, false);
            Double childPrecision = getPrecisionSa(queries, focusClass, false, bestChild);
            if (bestChild == null) { return currentConfig; }
            else currentConfig = bestChild;

            System.out.println("\n-----------------------------------");
            System.out.println("New current config set. The current config (press enter):");
            currentConfig.prettyPrint();
            System.out.println("\n-----------------------------------");
            System.out.println("cost/precision: ");
            System.out.println(childCost + "\t" + childPrecision);
        }
    }


    // just pick random to extend until too large
    public static Configuration searchForConfig3Random(List<QueryGraph> queries, String focusClass, boolean exact, double maxCost) throws Exception {

        Configuration smallestConfig = new Configuration();
        QueryVariable root = new QueryVariable("v1", focusClass);
        smallestConfig.addVertex(root);
        smallestConfig.setRoot(root);

        Configuration currentConfig = smallestConfig;
        while (true) {
            System.out.println("-------------------");
            System.out.println("current config:");
            System.out.println(currentConfig);
            currentConfig.prettyPrint();

            Set<Configuration> childConfigs = getChildrenConfigTreesSimple(currentConfig);
            Configuration child = pickRandomConfigFromSet(childConfigs);

            Double childCost = getConfigTreeCostCells(child, false);
            Double childPrecision = getPrecisionSa(queries, focusClass, false, child);

            System.out.println(childCost + "\t" + childPrecision);

            if (childCost > maxCost) {
                return currentConfig;
            }

            currentConfig = child;
        }
    }









    // here we do the full search for a whole config maps
    // I.e. we have expanded to full config collection, set.
    public static Map<String, Set<Configuration>> configFinderFullMCTS(List<QueryGraph> queries, boolean exact, double maxCost) throws Exception {
        System.out.println("max cost " + maxCost);

        Map<String, Set<Configuration>> bestMap = new HashMap<String, Set<Configuration>>();
        double bestCost = 0.0;
        double bestPrec = 0.0;

        // Make starting config
        Map<String, Set<Configuration>> rootMapNode = new HashMap<String, Set<Configuration>>();
        for (NavigationGraphNode v : navigationGraph.vertexSet()) {
            if (v instanceof ClassNode) {
                Configuration smallestConfig1 = new Configuration();
                QueryVariable root1 = new QueryVariable("v0", v.getLabel());
                smallestConfig1.addVertex(root1);
                smallestConfig1.setRoot(root1);

                Configuration smallestConfig2 = new Configuration();
                QueryVariable root2 = new QueryVariable("v0", v.getLabel());
                smallestConfig2.addVertex(root2);
                smallestConfig2.setRoot(root2);

                Set<Configuration> set = new HashSet<Configuration>();
                set.add(smallestConfig1);
                set.add(smallestConfig2);
                rootMapNode.put(v.getLabel(), set);
            }
        }

        // Representing the search tree
        Map<Map<String, Set<Configuration>>, Map<String, Set<Configuration>>> parent = new HashMap<Map<String, Set<Configuration>>, Map<String, Set<Configuration>>>();
        Map<Map<String, Set<Configuration>>, Set<Map<String, Set<Configuration>>>> children = new HashMap<Map<String, Set<Configuration>>, Set<Map<String, Set<Configuration>>>>();

        // Keeping track of the total and count
        Map<Map<String, Set<Configuration>>, Double> total = new HashMap<Map<String, Set<Configuration>>, Double>();
        Map<Map<String, Set<Configuration>>, Double> max = new HashMap<Map<String, Set<Configuration>>, Double>();
        Map<Map<String, Set<Configuration>>, Integer> count = new HashMap<Map<String, Set<Configuration>>, Integer>();


        total.put(rootMapNode, 0.0);
        max.put(rootMapNode, 0.0);
        count.put(rootMapNode, 0);

        // main loop doing selection, expansion, rollouts and backpropagation
        int mcCounter = 0;
        while(true) {
            mcCounter += 1;
            System.out.println("c: " + mcCounter + " precision/cost: " + bestPrec + "/" + bestCost );

            long a = System.nanoTime();
            Map<String, Set<Configuration>> selectedLeafNode = rootMapNode;
            while(true) {
                Set<Map<String, Set<Configuration>>> childrenOfCurrentNode = children.getOrDefault(selectedLeafNode, null);
                if (childrenOfCurrentNode == null) {
                    break;
                }
                else {
                    Map<String, Set<Configuration>> pickedChild = pickCleverConfigMapFromSet(childrenOfCurrentNode, total, max, count, parent);
                    selectedLeafNode = pickedChild;
                }
            }
            long b = System.nanoTime();
            Set<Map<String, Set<Configuration>>> newChildren = getChildrenForestMapsSimple(selectedLeafNode);

            // remove new children with already parents
            long c = System.nanoTime();
            Set<Map<String, Set<Configuration>>> newChildrenAccepted = new HashSet<Map<String, Set<Configuration>>>();
            for (Map<String, Set<Configuration>> child : newChildren) {
                Map<String, Set<Configuration>> par = parent.get(child);
                if (par == null) {
                    newChildrenAccepted.add(child);
                }
            }
            long d = System.nanoTime();


            for (Map<String, Set<Configuration>> child : newChildrenAccepted) parent.put(child, selectedLeafNode);
            children.put(selectedLeafNode, newChildrenAccepted);
            // select one of them to be the node to roll out from
            // Configuration rolloutNode = pickRandomConfigFromSet(newChildren);
            long e = System.nanoTime();
            Map<String, Set<Configuration>> rolloutNode = pickCleverConfigMapFromSet(newChildrenAccepted, total, max, count, parent);
            long f = System.nanoTime();

            // rollout
            List<Double> costAndPrecRollout = getMCTreeSearchMaxForMaps(rolloutNode, 3, 1, maxCost, queries, exact);
            long g = System.nanoTime();
            double rolloutPrecision = costAndPrecRollout.get(1);
            double rolloutCost = costAndPrecRollout.get(0);

            // just update best prec and cost after a rollout
            if (rolloutPrecision > bestPrec) {
                bestPrec = rolloutPrecision;
                bestCost = rolloutCost;
                bestMap = rolloutNode;
                System.out.println("rolloutnode: " + bestMap);
                System.out.println("New Best precision/cost: " + bestPrec + "/" + bestCost );

                writeStringToFile(bestCost + "\t" + bestPrec + "\t" + rolloutNode + "\n", "results-mc.tsv");
                System.out.println("parent " + parent.size());
                System.out.println("children " + children.size());
                System.out.println("total " + total.size());
            }

            // backpropagation
            Map<String, Set<Configuration>> backPropCurrent = rolloutNode;
            double oldBestPrec = max.getOrDefault(rootMapNode, 0.0);
            while(true) {
                double newMax = Math.max(rolloutPrecision, max.getOrDefault(backPropCurrent, 0.0));
                max.put(backPropCurrent, newMax);

                double newTotal = rolloutPrecision + total.getOrDefault(backPropCurrent, 0.0);
                total.put(backPropCurrent, newTotal);

                int newCount = count.getOrDefault(backPropCurrent, 0) + 1;
                count.put(backPropCurrent, newCount);

                // update node in backpropagation
                Map<String, Set<Configuration>> parentConfig = parent.get(backPropCurrent);
                if (parentConfig == null) break;
                else { backPropCurrent = parentConfig; }
            }

            System.out.println(rootMapNode + ": " + total.get(rootMapNode) + "/" + max.get(rootMapNode) + "/" + count.get(rootMapNode));

            long h = System.nanoTime();
        }
    }



    // This is quite slow after 32k iterations, probably due to memory issues. 
    public static Configuration configFinderMCTS(List<QueryGraph> queries, String focusClass, boolean exact, double maxCost) throws Exception {

        // Representing the search tree
        Map<Configuration, Configuration> parent = new HashMap<Configuration, Configuration>();
        Map<Configuration, Set<Configuration>> children = new HashMap<Configuration, Set<Configuration>>();

        // Keeping track of the total and count
        Map<Configuration, Double> total = new HashMap<Configuration, Double>();
        Map<Configuration, Double> max = new HashMap<Configuration, Double>();
        Map<Configuration, Integer> count = new HashMap<Configuration, Integer>();


        // Initate the root node
        Configuration rootConfigNode = new Configuration();
        QueryVariable rootVar = new QueryVariable("v1", focusClass);
        rootConfigNode.addVertex(rootVar);
        rootConfigNode.setRoot(rootVar);

        total.put(rootConfigNode, 0.0);
        max.put(rootConfigNode, 0.0);
        count.put(rootConfigNode, 0);

        // main loop doing selection, expansion, rollouts and backpropagation
        int mcCounter = 0;
        while(true) {
            mcCounter += 1;
            if (mcCounter > 100000) break;

            System.out.println("\nSELECT");
            // selection
            Configuration selectedLeafNode = rootConfigNode;
            while(true) {
                Set<Configuration> childrenOfCurrentNode = children.getOrDefault(selectedLeafNode, null);
                if (childrenOfCurrentNode == null) break;
                else {
                    // Configuration pickedChild = pickRandomConfigFromSet(childrenOfCurrentNode);
                    Configuration pickedChild = pickCleverConfigFromSet(childrenOfCurrentNode, total, max, count, parent);
                    selectedLeafNode = pickedChild;
                }
            }
            System.out.println("picked leaf node L: " + selectedLeafNode);
            System.out.println("it has total of " + total.get(selectedLeafNode));

            // expand
            System.out.println("\nEXPAND");
            Set<Configuration> newChildren = getChildrenConfigTreesCanonical(selectedLeafNode);
            // Add new config nodes to search tree:
            for (Configuration child : newChildren) parent.put(child, selectedLeafNode);
            children.put(selectedLeafNode, newChildren);
            // select one of them to be the node to roll out from
            Configuration rolloutNode = pickCleverConfigFromSet(newChildren, total, max, count, parent);
            System.out.println("Expansion is done. Rolloutnode, or C is selected: " + rolloutNode);

            // rollout
            System.out.println("\nROLLOUT");
            // rolloutNode.prettyPrint();
            List<Double> costAndPrecRollout = getMCTreeSearchMax(rolloutNode, 1000, 1, maxCost, queries, exact);
            System.out.println("did rollout, what was the result?");
            System.out.println(costAndPrecRollout);
            double rolloutPrecision = costAndPrecRollout.get(1);

            System.out.println("just printing children of root");
            Set<Configuration> cs = children.get(rootConfigNode);
            for (Configuration child : cs) {
                System.out.println(child + ": " + total.get(child) + "/" + max.get(child) + "/" + count.get(child));
            }

            // backpropagation
            System.out.println("\nBACKPROPAGATE");
            Configuration backPropCurrent = rolloutNode;
            while(true) {
                double newMax = Math.max(rolloutPrecision, max.getOrDefault(backPropCurrent, 0.0));
                max.put(backPropCurrent, newMax);

                double newTotal = rolloutPrecision + total.getOrDefault(backPropCurrent, 0.0);
                total.put(backPropCurrent, newTotal);

                int newCount = count.getOrDefault(backPropCurrent, 0) + 1;
                count.put(backPropCurrent, newCount);
                System.out.println("update node: " + backPropCurrent + " " + newTotal + "/" + newMax + "/" + newCount);

                // update node in backpropagation
                Configuration parentConfig = parent.get(backPropCurrent);
                if (parentConfig == null) break;
                else { backPropCurrent = parentConfig; }
            }
        }
        return null;
    }


    // Return a random configuration from the set of configurations.
    public static Map<String, Set<Configuration>> pickRandomMapFromSet(Set<Map<String, Set<Configuration>>> zs) throws Exception {
        int size = zs.size();
        int item = new Random().nextInt(size); 
        int i = 0;
        for(Map<String, Set<Configuration>> obj : zs) {
            if (i == item) return obj;
            i++;
        }
        return null;
    }


    // Return a random configuration from the set of configurations.
    public static Configuration pickRandomConfigFromSet(Set<Configuration> zs) throws Exception {
        int size = zs.size();
        int item = new Random().nextInt(size); 
        int i = 0;
        for(Configuration obj : zs) {
            if (i == item) return obj;
            i++;
        }
        return null;
    }

    // attempt to pick a more clever child based on mcts values.
    public static Configuration pickCleverConfigFromSet(Set<Configuration> childrenOfCurrentNode, Map<Configuration, Double> total, Map<Configuration, Double> max, Map<Configuration, Integer> count, Map<Configuration, Configuration> parent) {
        double bestValue = 0.0;
        Configuration bestConfig = null;
        for (Configuration child : childrenOfCurrentNode) {
            // If have not been rolled before, choose it now!
            if (count.getOrDefault(child, 0) == 0) return child;
            double childTotal = total.getOrDefault(child, 0.0);
            int childCount = count.getOrDefault(child, 0);
            double childMax = max.getOrDefault(child, 0.0);
            double parentCount = count.get(parent.get(child));
            double value = childTotal/(double)childCount + 1.4*Math.sqrt(Math.log(parentCount)/childCount);
            if (value > bestValue) {
                bestValue = value;
                bestConfig = child;
            }
        }
        return bestConfig;
    }

    // attempt to pick a more clever child based on mcts values.
    public static Map<String, Set<Configuration>> pickCleverConfigMapFromSet(Set<Map<String, Set<Configuration>>> childrenOfCurrentNode, Map<Map<String, Set<Configuration>>, Double> total, Map<Map<String, Set<Configuration>>, Double> max, Map<Map<String, Set<Configuration>>, Integer> count, Map<Map<String, Set<Configuration>>, Map<String, Set<Configuration>>> parent) {
        double bestValue = 0.0;
        Map<String, Set<Configuration>> bestConfig = null;
        for (Map<String, Set<Configuration>> child : childrenOfCurrentNode) {
            // If have not been rolled before, choose it now!
            if (count.getOrDefault(child, 0) == 0) {
                return child;
            }
            double childTotal = total.getOrDefault(child, 0.0);
            int childCount = count.getOrDefault(child, 0);
            double childMax = max.getOrDefault(child, 0.0);
            double parentCount = count.get(parent.get(child));
            double value = childTotal/(double)childCount + 0.01*Math.sqrt(Math.log(parentCount)/childCount);
            if (value > bestValue) {
                bestValue = value;
                bestConfig = child;
            }
        }
        return bestConfig;
    }


    // attempt to pick a more clever child based on mcts values.
    public static Configuration selectOnEffect(Set<Configuration> candidates, List<QueryGraph> queries, String focusClass, boolean exact ) {
        double bestValue = 0.0;
        Configuration bestConfig = null;
        for (Configuration child : candidates) {
            System.out.println("considering candidate " + child);
            Double cost = getConfigTreeCostCells(child, exact);
            Double precision = getPrecisionSa(queries, focusClass, exact, child);
            Double effect = precision/cost;
            System.out.println("effect = " + effect);
            if (effect > bestValue) {
                bestValue = effect;
                bestConfig = child;
            }
        }
        return bestConfig;
    }


    // here we discover the tree search
    public static Configuration searchForConfig2WannabeMCFirstTry(List<QueryGraph> queries, String focusClass, boolean exact, double maxCost) throws Exception {
        Configuration smallestConfig = new Configuration();
        QueryVariable root = new QueryVariable("v1", focusClass);
        smallestConfig.addVertex(root);
        smallestConfig.setRoot(root);

        Configuration currentConfig = smallestConfig;
        while (true) {
            Set<Configuration> childConfigs = getChildrenConfigTreesSimple(currentConfig);

            double bestPrecision = 0.0;
            Configuration bestConfig = null;

            System.out.println("Checking all children of smallest config");
            System.out.println("There are now " + childConfigs.size() + " new children configs to check");
            int childCounter = 0;
            for (Configuration childConfig : childConfigs) {
                childCounter += 1;
                System.out.println("child config " + childCounter + "/" + childConfigs.size());
                long a = System.nanoTime();

                Double childCost = getConfigTreeCostCells(childConfig, false);
                long b = System.nanoTime();

                if (childCost > maxCost) continue;
                // going as deep as 50 100 times is very slow. I think deep ones are the slowest.
                // Potential improvement. Binary search to get largest. or find out what takes time. It could just be the large numbers used in ansOptEx.
                long c = System.nanoTime();
                List<Double> costAndPrec = getMCTreeSearchMax(childConfig, 1000, 10, maxCost, queries, exact);

                Double costMC = costAndPrec.get(0);
                Double precMC = costAndPrec.get(1);

                if (precMC > bestPrecision) {
                    bestPrecision = precMC;
                    bestConfig = childConfig;
                }
            }

            // terminate when there were no more extension to do.
            if (bestConfig == null) {
                return currentConfig;
            }

            System.out.println("-------Best--------------------------");
            System.out.println("next config: " + bestConfig);
            System.out.println("with best future MC precision: " + bestPrecision);
            System.out.println("calculating...");
            Double currentCost = getConfigTreeCostCells(bestConfig, false);
            Double currentPrec = getPrecisionSa(queries, focusClass, false, bestConfig);

            writeStringToFile(currentCost + "\t" + currentPrec + "\t" + bestPrecision + "\t" + bestConfig + "\n", "results-wannabe-mc-10.tsv");

            currentConfig = bestConfig;
        }
    }


    // Random version with full config maps
    // pick random extension until max cost is reached
    // this random tends to pick depth first search, or at least it makes very deep configs, and that is not very useful.
    public static Map<String, Set<Configuration>> searchForConfigFull3Random(List<QueryGraph> queries, boolean exact, double maxCost) throws Exception {

        Map<String, Set<Configuration>> smallestMap = new HashMap<String, Set<Configuration>>();
        Map<String, Set<Configuration>> currentMap = smallestMap;
        while (true) {

            Set<Map<String, Set<Configuration>>> childrenMaps = getChildrenForestMapsSimple(currentMap);
            Map<String, Set<Configuration>> childMap = pickRandomMapFromSet(childrenMaps);

            Double childCost = getConfigMapCostCells(childMap, false);
            Double childPrecision = getPrecisionSa(queries, childMap, false);
            writeStringToFile(childCost + "\t" + childPrecision + "\t" + childMap + "\n", "results-random.txt");
            System.out.println(childCost + "\t" + childPrecision);

            if (childCost > maxCost) {
                return currentMap;
            }
            currentMap = childMap;
        }
    }

    // Here we do a wannabe mc search, in order to find good extensions
    // list of queries is all queries we test over. They are needed to calculate precision
    // boolean exact, if we should use exact cost and precision. this requries wikidata database setup
    // maxcost, is the max cost we search for. If the cost is higher than this, we terminate.
    public static Map<String, Set<Configuration>> searchForConfigFull2WannabeMCFirstTry(List<QueryGraph> queries, boolean exact, double maxCost) throws Exception {

        // this has cost and precision zero
        // define a smallest map. This map is so small that it does not take any space at all I think
        // make two at a time, to help it to expand.
        Map<String, Set<Configuration>> smallestMap = new HashMap<String, Set<Configuration>>();
        for (NavigationGraphNode v : navigationGraph.vertexSet()) {
            if (v instanceof ClassNode) {
                Configuration smallestConfig1 = new Configuration();
                QueryVariable root1 = new QueryVariable("v0", v.getLabel());
                smallestConfig1.addVertex(root1);
                smallestConfig1.setRoot(root1);

                Set<Configuration> set = new HashSet<Configuration>();
                set.add(smallestConfig1);
                smallestMap.put(v.getLabel(), set);
            }
        }

        String bestSelection = "prec";

        // start on the search.
        Map<String, Set<Configuration>> currentMap = smallestMap;
        int s = 0;
        Double currentCost = 0.0;
        Double currentPrec = 0.0;
        List<Integer> childrenMapsSize = new ArrayList<Integer>();
        while (s < 100000) {
            s+= 1;
            Set<Map<String, Set<Configuration>>> childrenMaps = getChildrenForestMapsSimple(currentMap);
            System.out.println("childre map size: " + childrenMaps.size());
            childrenMapsSize.add(childrenMaps.size());

            Integer su = 0;
            for (Integer i : childrenMapsSize){
                su  += i;
            }
            System.out.println("avg:" + su/s);

            double bestPrecision = 0.0;
            double bestScore = 0.0;
            Map<String, Set<Configuration>> bestMap = null;

            System.out.println("Checking all children of current map");
            System.out.println("There are now " + childrenMaps.size() + " new children maps to check");
            int childCounter = 0;

            for (Map<String, Set<Configuration>> childMap : childrenMaps) {
                childCounter += 1;
                Double childCost =  getConfigMapCostCells(childMap, false);

                if (childCost > maxCost) continue;
                // going as deep as 50 100 times is very slow. I think deep ones are the slowest.
                // Potential improvement. Binary search to get largest. or find out what takes time. It could just be the large numbers used in ansOptEx.
                // steps / # rollouts
                // 0/1 for greedy
                // 1,10 otherwise
                List<Double> costAndPrec = getMCTreeSearchMaxForMaps(childMap ,1 , 10, maxCost, queries, exact);

                Double costMC = costAndPrec.get(0);
                Double precMC = costAndPrec.get(1);

                System.out.println("child map " + childCounter + "/" + childrenMaps.size() + ":\t" + precMC + " - " + costMC);

                if (bestSelection.equals("prec")) {
                    if (precMC >= bestPrecision) {
                        bestPrecision = precMC;
                        bestMap = childMap;
                    }
                }
            }

            // terminate when there were no more extension to do.
            if (bestMap == null) {
                return currentMap;
            }

            System.out.println("-------WINNER This round --------------------------");
            System.out.println("next config: " + bestMap);
            System.out.println("with best future MC precision: " + bestPrecision);
            System.out.println("calculating...");
            currentCost = getConfigMapCostCells(bestMap, false);
            currentPrec = getPrecisionSa(queries, bestMap, false);
            long stepTime = System.nanoTime();
            double time = (stepTime - startTime) / 1e6;  // make into seconds
            System.out.println("time so far sec: " + time);


            System.out.println("And the actual (est) cost of this config atm is (# cells): " + currentCost);
            System.out.println("And the actual (est) precision of this config atm is: " + currentPrec);
            writeStringToFile(currentCost + "\t" + currentPrec + "\t" + bestPrecision + "\t" + time + "\t" + bestMap + "\n", "results-explore.tsv");

            currentMap = bestMap;
        }
        return null;
    }

    // just a simple function to write to file
    public static void writeStringToFile(String data, String filename) {
        try {

            File file = new File(filename);
            FileWriter fr = new FileWriter(file, true);
            fr.write(data);
            fr.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    // Given a config, explore larger configs based on this, and see if they are useful.
    // steps is how many times we should add a new variable to config.
    // numberOfRollouts is how many rollouts to do.
    public static List<Double> getMCTreeSearchMaxForMaps(Map<String, Set<Configuration>> startMap, int steps, int numberOfRollouts, double maxCost, List<QueryGraph> qs, boolean exact ) throws Exception {
        Double costOfMaxPrecision = 0.0;
        Double maxPrecision = 0.0;
        for (int rolloutInd = 0 ; rolloutInd < numberOfRollouts ; rolloutInd++) {
            long a = System.nanoTime();
            System.out.println("r:" + rolloutInd );
            Map<String, Set<Configuration>> currentMap = startMap;
            Double costForCurrentMap = getConfigMapCostCells(currentMap, false);

            Double bestCostForRollout = costForCurrentMap;
            Map<String, Set<Configuration>> bestConfigMapForRollout = startMap;
            long b = System.nanoTime();
            for (int stepInd = 0 ; stepInd < steps ; stepInd++) {
                long l1 = System.nanoTime();

                Set<Map<String, Set<Configuration>>> childrenMaps = getChildrenForestMapsSimple(currentMap);

                long l2 = System.nanoTime();
                // pick either of the methods below to do a rollout
                Map<String, Set<Configuration>> nextMap = pickRandomMapFromSet(childrenMaps);

                long l3 = System.nanoTime();
                Double costForRolloutNext = getConfigMapCostCells(nextMap, false);

                if (costForRolloutNext > maxCost) {
                    System.out.println(" reached cost max");
                    bestConfigMapForRollout = currentMap;
                    bestCostForRollout = costForCurrentMap;
                    break;
                }
                else {
                    // we are on the last one
                    if (stepInd == steps-1) {
                        bestConfigMapForRollout = nextMap;
                        bestCostForRollout = costForRolloutNext;
                        break;
                    }
                    else { 
                        currentMap = nextMap; 
                        costForCurrentMap = costForRolloutNext; 
                    }
                }
                long l4 = System.nanoTime();
            }
            long c = System.nanoTime();

            Double precisionForRollout = getPrecisionSa(qs, bestConfigMapForRollout, false);
            System.out.println("prec for rollout: " + precisionForRollout );

            if (precisionForRollout > maxPrecision) {
                maxPrecision = precisionForRollout;
                costOfMaxPrecision = bestCostForRollout;
            }
            long d = System.nanoTime();

        }
        List<Double> returnList = new ArrayList<Double>();
        returnList.add(costOfMaxPrecision);
        returnList.add(maxPrecision);
        return returnList;
    }



    // Given a config, explore larger configs based on this, and see if they are useful.
    // steps is how many times we should add a new variable to config.
    // numberOfRollouts is how many rollouts to do.
    public static List<Double> getMCTreeSearchMax(Configuration z, int steps, int numberOfRollouts, double maxCost, List<QueryGraph> qs, boolean exact ) throws Exception {
        System.out.println("\n--------------------------\nmax mc tree search start");
        String focusClass = z.getRoot().getType();
        Double costOfMaxPrecision = 0.0;
        Double maxPrecision = 0.0;
        for (int rolloutInd = 0 ; rolloutInd < numberOfRollouts ; rolloutInd++) {
            long a = System.nanoTime();
            Configuration currentZ = z;
            Double costForCurrentZ = getConfigTreeCostCells(currentZ, false);
            long b = System.nanoTime();

            Double bestCostForRollout = getConfigTreeCostCells(currentZ, false);
            Configuration bestConfigForRollout = null;
            for (int stepInd = 0 ; stepInd < steps ; stepInd++) {

                Set<Configuration> childConfigs = getChildrenConfigTreesSimple(currentZ);

                // pick either of the methods below to do a rollout
                Configuration nextZ = pickRandomConfigFromSet(childConfigs);

                Double costForRolloutNext = getConfigTreeCostCells(nextZ, false);

                if (costForRolloutNext > maxCost) {
                    System.out.println(" reached cost max");
                    bestConfigForRollout = currentZ;
                    bestCostForRollout = costForCurrentZ;
                    break;
                }
                else {
                    // we are on the last one
                    if (stepInd == steps-1) {
                        bestConfigForRollout = nextZ;
                        bestCostForRollout = costForRolloutNext;
                        break;
                    }
                    else { currentZ = nextZ; costForCurrentZ = costForRolloutNext; }
                }
            }

            long c = System.nanoTime();
            Double precisionForRollout = getPrecisionSa(qs, focusClass, false, bestConfigForRollout);


            long d = System.nanoTime();
            if (precisionForRollout > maxPrecision) {
                maxPrecision = precisionForRollout;
                costOfMaxPrecision = bestCostForRollout;
            }
            long e = System.nanoTime();

        }
        List<Double> returnList = new ArrayList<Double>();
        returnList.add(costOfMaxPrecision);
        returnList.add(maxPrecision);
        return returnList;
    }















    // Get the set of all possible ways to extend a config into a superset config. This is the "children" of the config.
    // This version does not allow to add a new edge unless it is higher in the alphabet than its siblings
    // This ensures simple configurations, and canonical representation of configs
    public static Set<Configuration> getChildrenConfigTreesCanonical(Configuration parentConfig) {
        Set<Configuration> returnSet = new HashSet<Configuration>(); // Set of all new configs to return

        // Extend from an origin variable
        for (QueryVariable originVar : parentConfig.vertexSet()) {
            if (isObjectVariable(originVar, navigationGraph)) {  // Can only extend from object variables
                String originType = originVar.getType();
                String originLabel = originVar.getLabel();

                // Find the corresponding vertex in ng
                for (NavigationGraphNode ngOrigin : navigationGraph.vertexSet()) {
                    if (ngOrigin instanceof ClassNode) {
                        if (ngOrigin.getLabel().equals(originType)) {
                            ClassNode ngOriginVertex = (ClassNode)ngOrigin;

                            // for each outgoing edge in ng, check if we should expand the parentConfig
                            for (PropertyEdge pe : navigationGraph.outgoingEdgesOf(ngOriginVertex)) {
                                NavigationGraphNode ngTargetVertex = navigationGraph.getEdgeTarget(pe);


                                boolean addedLargerBefore = false;
                                for (PropertyEdge otherOutgoingEdge : parentConfig.outgoingEdgesOf(originVar)) {
                                    QueryVariable otherTargetVertex = parentConfig.getEdgeTarget(otherOutgoingEdge);
                                    if (pe.getLabel().compareTo(otherOutgoingEdge.getLabel()) < 0){
                                        addedLargerBefore = true;
                                    }
                                    if (pe.getLabel().compareTo(otherOutgoingEdge.getLabel()) == 0){
                                        if (ngTargetVertex.getLabel().compareTo(otherTargetVertex.getType()) <= 0){
                                            addedLargerBefore = true;
                                        }
                                    }
                                }

                                if (!addedLargerBefore) {
                                    Configuration copy = new Configuration(parentConfig);
                                    String exVarName = copy.getFreeVariableName();
                                    QueryVariable exVar = new QueryVariable(exVarName, ngTargetVertex.getLabel());
                                    copy.addVertex(exVar);
                                    copy.addEdge(copy.getVariable(originLabel), exVar, new PropertyEdge(pe.getLabel()) );
                                    returnSet.add(copy);
                                }
                            }
                        }
                    }
                }
            }
        }
        return returnSet;
    }





    // Only allow to extend if not this edge exists yet. I.e. we restrict to simple configs
    public static Set<Configuration> getChildrenConfigTreesSimple(Configuration parentConfig) {
        Set<Configuration> returnSet = new HashSet<Configuration>(); // Set of all new configs to return

        Map<QueryVariable, QueryVariable> parentMap = parentConfig.calculateParentMap();

        // Extend from an origin variable
        for (QueryVariable originVar : parentConfig.vertexSet()) {
            if (isObjectVariable(originVar, navigationGraph)) {  // Can only extend from object variables
                String originType = originVar.getType();
                String originLabel = originVar.getLabel();

                // construct path so far from root to origin var
                String pathToOrigin = "";
                pathToOrigin = originType;
                QueryVariable c = originVar;
                while (parentMap.getOrDefault(c, null) != null) {
                    QueryVariable sourceVar = parentMap.get(c);
                    String sourceClass = sourceVar.getType();
                    Set<PropertyEdge> eForward = parentConfig.getAllEdges(sourceVar, c); // get all edges from the parent to this variable
                    PropertyEdge edgeBetween = null;
                    for (PropertyEdge e : eForward) { edgeBetween = e; } 

                    String edgeLabel = edgeBetween.getLabel();
                    pathToOrigin = sourceClass + " " + edgeLabel + " " + pathToOrigin;
                    if (pathToOrigin.length() > 1000) {
                        System.out.println("stop, here too long");
                        System.out.println("the config");
                        System.out.println(parentConfig);
                        System.out.println("the parent map");
                        System.out.println(parentMap);
                        try{
                            System.in.read();
                        }
                        catch (Exception e) {}
                    }

                    c = sourceVar;
                }


                // Find the corresponding vertex in ng
                for (NavigationGraphNode ngOrigin : navigationGraph.vertexSet()) {
                    if (ngOrigin instanceof ClassNode) {
                        if (ngOrigin.getLabel().equals(originType)) {
                            ClassNode ngOriginVertex = (ClassNode)ngOrigin;

                            // for each outgoing edge in ng, check if we should expand the parentConfig
                            for (PropertyEdge pe : navigationGraph.outgoingEdgesOf(ngOriginVertex)) {
                                NavigationGraphNode ngTargetVertex = navigationGraph.getEdgeTarget(pe);

                                // if the path we construct is not in query log, then we skip it
                                String pathToNewVariable = pathToOrigin + " " + pe.getLabel() + " " + navigationGraph.getEdgeTarget(pe);
                                if (!pathWeight.containsKey(pathToNewVariable)) {
                                    continue;
                                }

                                // check if added before
                                boolean addedSameBefore = false;
                                for (PropertyEdge otherOutgoingEdge : parentConfig.outgoingEdgesOf(originVar)) {
                                    QueryVariable otherTargetVertex = parentConfig.getEdgeTarget(otherOutgoingEdge);
                                    if (pe.getLabel().equals(otherOutgoingEdge.getLabel())){
                                        if (ngTargetVertex.getLabel().equals(otherTargetVertex.getType())){
                                            addedSameBefore = true;
                                        }
                                    }
                                }

                                if (!addedSameBefore) {
                                    Configuration copy = new Configuration(parentConfig);
                                    String exVarName = copy.getFreeVariableName();
                                    QueryVariable exVar = new QueryVariable(exVarName, ngTargetVertex.getLabel());
                                    copy.addVertex(exVar);
                                    copy.addEdge(copy.getVariable(originLabel), exVar, new PropertyEdge(pe.getLabel()) );
                                    returnSet.add(copy);
                                }
                            }
                        }
                    }
                }
            }
        }
        return returnSet;
    }



    // get children config families of a config family
    public static Set<Set<Configuration>> getChildrenConfigForestsSimple(Set<Configuration> parentConfigFamily, String focusClass) {

        Set<Set<Configuration>> returnSet = new HashSet<Set<Configuration>>(); // Set of all new config families to return

        // either add a new very small config to the family
        Set<Configuration> addNewC = new HashSet<Configuration>();
        for (Configuration config : parentConfigFamily ) addNewC.add(config);
        Configuration smallestC = new Configuration();
        QueryVariable v1 = new QueryVariable("v0", focusClass);
        smallestC.addVertex(v1);
        smallestC.setRoot(v1);
        addNewC.add(smallestC);
        returnSet.add(addNewC);

        // or extend one of the existing ones
        for (Configuration config : parentConfigFamily ) { // choose which config to extend
            Set<Configuration> childrenConfigs = getChildrenConfigTreesSimple(config); // get all its successors
            for (Configuration childrenConfig : childrenConfigs) {

                // make a new config where config is replace with childrenConfig
                Set<Configuration> newC = new HashSet<Configuration>();
                for (Configuration c : parentConfigFamily) {
                    if (c.equals(config)) newC.add(childrenConfig);
                    else newC.add(c);
                }
                returnSet.add(newC);
            }
        }
        return returnSet;
    }


    // get children forest collections of a given forest map/collection
    // this used in the search for a good config
    public static Set<Map<String, Set<Configuration>>> getChildrenForestMapsSimple(Map<String, Set<Configuration>> parentForestMap) {

        Set<Map<String, Set<Configuration>>> returnMaps = new HashSet<Map<String, Set<Configuration>>>(); // Set of maps to return

        // either start on a new forest for a class without a forest already., should always be fine
        for (NavigationGraphNode classNode : navigationGraph.vertexSet()) {
            if (classNode instanceof ClassNode) {
                String classLabel = classNode.getLabel();
                Set<Configuration> existingForestForClass = parentForestMap.get(classLabel);
                if (existingForestForClass == null) {

                    // forest does not exist for this class yet. Can add it.
                    Map<String, Set<Configuration>> newMap = new HashMap<String, Set<Configuration>>();
                    for (Map.Entry<String,Set<Configuration>> entry : parentForestMap.entrySet()) {
                        newMap.put(entry.getKey(), entry.getValue());
                    }
                    Set<Configuration> newEmptyForest = new HashSet<Configuration>();
                    newMap.put(classLabel, newEmptyForest);
                    returnMaps.add(newMap);
                }
            }
        }

        // or grow one of the forests into a larger forest
        for (Map.Entry<String,Set<Configuration>> entry : parentForestMap.entrySet()) {
            String classLabel = entry.getKey();
            Set<Configuration> currentForest = entry.getValue(); // all configar til perso fesk

            Set<Set<Configuration>> childrenForests = getChildrenConfigForestsSimple(currentForest, classLabel);
            for (Set<Configuration> childrenForest : childrenForests) {
                // make a new forest where currentForest is replace with childrenForest
                Map<String, Set<Configuration>> newMap = new HashMap<String, Set<Configuration>>();

                for (Map.Entry<String,Set<Configuration>> e : parentForestMap.entrySet()) {
                    String cl = e.getKey();
                    Set<Configuration> cf = e.getValue();
                    if (e.equals(entry)) newMap.put(cl, childrenForest);
                    else newMap.put(cl, cf);
                }
                returnMaps.add(newMap);
            }
        }

        return returnMaps;
    }

































    /////////////////////////////////////////////////////////////
    //Generate the Sr and Sl configs
    /////////////////////////////////////////////////////////////


    // Generate the configuration where every local property gets its separate config
    // Generate the range-based configuration forest, given a focus class
    public static Set<Configuration> generateSrConfigForest(String focusClass, String includedProps) {

        boolean includeObj = false;
        boolean includeDat = false;
        if (includedProps == "obj") includeObj = true;
        if (includedProps == "dat") includeDat = true;
        if (includedProps == "objdat") {includeDat = true; includeObj = true;}


        Set<Configuration> returnSet = new HashSet<Configuration>();

        NavigationGraphNode ngRoot = null;
        for (NavigationGraphNode n : navigationGraph.vertexSet()) {
            if (n.getLabel().equals(focusClass)) ngRoot = n;
        }

        // outgoing edges
        for (PropertyEdge pe : navigationGraph.outgoingEdgesOf(ngRoot) ) {
            NavigationGraphNode target = navigationGraph.getEdgeTarget(pe);
            if ( ((target instanceof DatatypeNode) && includeDat) || ((target instanceof ClassNode) && includeObj) ){

                Configuration z = new Configuration();
                QueryVariable rootVariable = new QueryVariable(z.getFreeVariableName(), focusClass);
                z.addVertex(rootVariable);

                QueryVariable ex = new QueryVariable(z.getFreeVariableName(), target.getLabel());
                z.addVertex(ex);
                z.addEdge(rootVariable, ex, new PropertyEdge(pe.getLabel()));

                z.setRoot(rootVariable);
                returnSet.add(z);
            }
        }
        return returnSet;
    }


    // Generate the configuration where every local property is added. 
    public static Configuration generateSlConfigTree(String focusClass, String includedProps) {

        boolean includeObj = false;
        boolean includeDat = false;
        if (includedProps == "obj") includeObj = true;
        if (includedProps == "dat") includeDat = true;
        if (includedProps == "objdat") {includeDat = true; includeObj = true;}

        Configuration z = new Configuration(navigationGraph);
        NavigationGraphNode ngRoot = null;
        for (NavigationGraphNode n : navigationGraph.vertexSet()) {
            if (n.getLabel().equals(focusClass)) ngRoot = n;
        }
        QueryVariable rootVariable = new QueryVariable(z.getFreeVariableName(), focusClass);
        z.addVertex(rootVariable);
        z.setRoot(rootVariable);

        // outgoing edges
        for (PropertyEdge pe : navigationGraph.outgoingEdgesOf(ngRoot) ) {
            NavigationGraphNode target = navigationGraph.getEdgeTarget(pe);

            if ( ((target instanceof DatatypeNode) && includeDat) || ((target instanceof ClassNode) && includeObj) ){
                QueryVariable ex = new QueryVariable(z.getFreeVariableName(), target.getLabel());
                z.addVertex(ex);
                z.addEdge(rootVariable, ex, new PropertyEdge(pe.getLabel()));
            }
        }
        return z;
    }



    // generate a full map with sl configs of high level. The goal is to construct a config map which covers every other query.
    public static Map<String, Set<Configuration>> generateFullConfigMapByLevel(int level) {
        System.out.println("Making Config full level " + level + "...");
        Map<String, Set<Configuration>> returnMap = new HashMap<String, Set<Configuration>>();
        for (NavigationGraphNode n : navigationGraph.vertexSet()) {
            if (n instanceof ClassNode) {
                String labelOfClass = n.getLabel();
                System.out.println(labelOfClass);
                Configuration c = generateFullConfigByClassAndLevel(labelOfClass, level);
                Set<Configuration> tmp = new HashSet<Configuration>();
                tmp.add(c);
                returnMap.put(labelOfClass, tmp);
            }
        }
        return returnMap;
    }


    // Generate the configuration where every local property is added. 
    public static Configuration generateFullConfigByClassAndLevel(String focusClass, int level) {
        Configuration z = new Configuration();
        NavigationGraphNode ngRoot = null;
        for (NavigationGraphNode n : navigationGraph.vertexSet()) {
            if (n.getLabel().equals(focusClass)) ngRoot = n;
        }
        QueryVariable rootVariable = new QueryVariable(z.getFreeVariableName(), focusClass);
        z.addVertex(rootVariable);
        z.setRoot(rootVariable);


        List<QueryVariable> nodesToExpand = new ArrayList<QueryVariable>();
        nodesToExpand.add(rootVariable);

        for (int i = 0 ; i < level ; i++) {
            System.out.println("level " + i);
            List<QueryVariable> newNodesToExpand = new ArrayList<QueryVariable>();

            for (QueryVariable variable : nodesToExpand) {

                NavigationGraphNode typeInNG = null;
                for (NavigationGraphNode n : navigationGraph.vertexSet()) {
                    if (n.getLabel().equals(variable.getType())) typeInNG = n;
                }

                // outgoing edges
                for (PropertyEdge pe : navigationGraph.outgoingEdgesOf(typeInNG) ) {
                    NavigationGraphNode target = navigationGraph.getEdgeTarget(pe);

                    QueryVariable ex = new QueryVariable(z.getFreeVariableName(), target.getLabel());
                    z.addVertex(ex);
                    z.addEdge(variable, ex, new PropertyEdge(pe.getLabel()));

                    newNodesToExpand.add(ex);
                    System.out.println(z.vertexSet().size());
                    if (z.vertexSet().size() > 1500) {
                        System.out.println("reached max: 1500");
                        return z;
                    }
                }
            }
            nodesToExpand = newNodesToExpand;
        }

        return z;
    }





    // generate the full Sl config map for all classes in ng
    // includedProps is "dat", objdat or obj. It is whether we should include all data prpos, all obj props, or both
    public static Map<String, Set<Configuration>> generateSlConfigMap(String onlyFocusClass, String includedProps) {
        Map<String, Set<Configuration>> returnMap = new HashMap<String, Set<Configuration>>();
        for (NavigationGraphNode n : navigationGraph.vertexSet()) {
            if (n instanceof ClassNode) {
                String classLabel = n.getLabel();
                if (onlyFocusClass == null || classLabel.equals(onlyFocusClass)) {
                    Set<Configuration> forest = new HashSet<Configuration>();
                    forest.add(generateSlConfigTree(classLabel, includedProps));
                    returnMap.put(classLabel, forest);
                }
            }
        }
        return returnMap;
    }


    // generate the full Sr config map for all classes in ng
    // if onlyFocusClass is not null, then that is the only focus class to add.
    // if onlyDataProps = true, then only data properties are added.
    public static Map<String, Set<Configuration>> generateSrConfigMap(String onlyFocusClass, String includeProps) {
        Map<String, Set<Configuration>> returnMap = new HashMap<String, Set<Configuration>>();
        for (NavigationGraphNode n : navigationGraph.vertexSet()) {
            if (n instanceof ClassNode) {
                String classLabel = n.getLabel();
                if (onlyFocusClass == null || classLabel.equals(onlyFocusClass)) {
                    returnMap.put(classLabel, generateSrConfigForest(classLabel, includeProps));
                }
            }
        }
        return returnMap;
    }




































































    ///////////////////////////////////////////////////////
    // HELPER CLASSES
    ///////////////////////////////////////////////////////

    // return true if a string is a class in NG. Return false if it is a Data property. Return null if none of them.
    public static Boolean isClass(String classLabel) {
        for (NavigationGraphNode v : navigationGraph.vertexSet()) {
            if (v.getLabel().equals(classLabel)) {
                if (v instanceof ClassNode) return true;
                if (v instanceof DatatypeNode) return false;
            }
        }
        return null;
    }


    // Helper class to just check if a config has the given extension pair
    public static boolean doesConfigContainExtensionPair(Configuration z, String propertyLabel, String extensionClass) {
        for (PropertyEdge pe : z.outgoingEdgesOf(z.getRoot())) {
            if (pe.getLabel().equals(propertyLabel)) {
                if (z.getEdgeTarget(pe).getType().equals(extensionClass)) return true;
            }
        }
        return false;
    }


    // Go over the navigation graph and collect all needed statistics.
    public static void gatherAndCacheAllStats() {
        System.out.println("Gather and cache all data needed:");
        for (NavigationGraphNode n : navigationGraph.vertexSet()) {
            Integer a = getStatCF(n.getLabel());
        }
        System.out.println("done verticies");
        for (PropertyEdge e : navigationGraph.edgeSet()) {
            String label = e.getLabel();
            NavigationGraphNode source = navigationGraph.getEdgeSource(e);
            NavigationGraphNode target = navigationGraph.getEdgeTarget(e);
            Integer a = getStatPF(source.getLabel(), e.getLabel(), target.getLabel());
            Integer b = getStatDT(source.getLabel(), e.getLabel(), target.getLabel());
            Integer c = getStatDS(source.getLabel(), e.getLabel(), target.getLabel());
        }
    }


    // Print the size profile of queries
    public static void printSizeProfile(List<QueryGraph> queries) throws Exception {
        System.out.println("Size of queries: and the sum of weights in each of them.");
        Map<Integer, Double> sumWeights = new HashMap<Integer, Double>();
        for (QueryGraph qu : queries) {
            Integer key = qu.vertexSet().size();
            sumWeights.put(key, qu.getWeight() + sumWeights.getOrDefault(key, 0.0));
        }
        System.out.println(sumWeights);
    }


    // Given two counts of exact and estimated answers, calulate the error, which is estimated/exact
    public static Double calculateExactEstimateError(Double exact, Double estimate) {
        if (exact == null) return null;
        if (exact == 0 && estimate == 0) return 1.0;
        return estimate/exact;
    }


    // Idea here is to modify the navigation graph to also include inverses. 
    public static NavigationGraph makeNavigationGraphBidirectional(NavigationGraph ng) {
        NavigationGraph returnNavigationGraph = new NavigationGraph();
        for (NavigationGraphNode n : ng.vertexSet()) returnNavigationGraph.addVertex(n);
        for (PropertyEdge pe : ng.edgeSet()) {
            NavigationGraphNode source = ng.getEdgeSource(pe);
            NavigationGraphNode target = ng.getEdgeTarget(pe);
            if (source instanceof DatatypeNode) {
                System.out.println("Navigation graph is not good. Cannot have edge going out from Datatype");
            }
            else {
                returnNavigationGraph.addEdge(source, target, new PropertyEdge(pe.getLabel()));
                if (target instanceof ClassNode) {
                    returnNavigationGraph.addEdge(target, source, new PropertyEdge(pe.getLabel() + "^"));
                }
            }
        }
        return returnNavigationGraph;
    }






































    /////////////////////////////////////////////////////////////////////
    // COST OF CONFIGS
    /////////////////////////////////////////////////////////////////////

    // get cost of map of forests, i.e. a full configuration set
    public static Double getConfigMapCostCells(Map<String, Set<Configuration>> configMap, boolean exact) {
        Double costSum = 0.0;

        for (Map.Entry<String,Set<Configuration>> entry : configMap.entrySet()) {
            String key = entry.getKey();
            Set<Configuration> forest = entry.getValue();
            costSum += getConfigForestCostCells(forest, exact);
        } 
        return costSum;
    }

    // get the cost of a config. set exact=true if we want to calculate exact cost. This is for a set of configs, where each is he same class
    public static Double getConfigForestCostCells(Set<Configuration> configForest, boolean exact) {
        Double costSum = 0.0;
        for (Configuration z : configForest) {
            costSum += getConfigTreeCostCells(z, exact);
        }
        return costSum;
    }

    // get the cost of a config. set exact=true if we want to calculate exact cost
    public static Double getConfigTreeCostCells(Configuration z, boolean exact) {
        // Here we count each cell in the config, but we ignore the first column, since it is always just true.
        Double rows =  getAnsOptEx(z, exact);
        Integer columns = z.vertexSet().size() - 1;
        Double cells =  rows*columns;
        return cells;
    }







    //////////////////////////////////////////////////////////////////
    // Now we can calculate precision for So, Sa and Sr
    // Extended to a list of queries, i.e. a query catalogue
    //////////////////////////////////////////////////////////////////


    // Version using a set of queries
    public static Double getPrecisionSoManyQueries(List<QueryGraph> qs, String focusClass, boolean exact) {
        return 1.0;
    }


    // Version using a set of queries Sr
    public static Double getPrecisionSrManyQueries(List<QueryGraph> qs, String focusClass, boolean exact) {
        // 1. Make the set of Sr configs
        Set<Configuration> srConfigs = generateSrConfigForest(focusClass, "objdat");

        // 2. use Sa to calculate precision
        return getPrecisionSa(qs, focusClass, exact, srConfigs);
    }


    public static Double getPrecisionSa(QueryGraph q, String propertyLabel, String extensionClass, boolean exact, Set<Configuration> zs) {

        double maxPrec = 0.0;
        for (Configuration z : zs) {
            Double prec = getPrecisionSa(q, propertyLabel, extensionClass, exact, z);
            if (prec > maxPrec) maxPrec = prec;
        }

        if (maxPrec < 0.5) {
            try {
            }
            catch(Exception e) {}
        }
        return maxPrec;
    }


    // Just one configuration
    public static Double getPrecisionSa(List<QueryGraph> qs, String focusClass, boolean exact, Configuration z) {
        Set<Configuration> zs = new HashSet<Configuration>(Arrays.asList(z));
        return getPrecisionSa(qs, focusClass, exact, zs);
    }

    // A object variable check outside of the querygraph.
    public static boolean isObjectVariable(QueryVariable var, NavigationGraph navigationGraph) {
        Set <String> allClassLabels = navigationGraph.getAllClasses();
        if (allClassLabels.contains(var.getType())) return true;
        return false;
    }


    // using a set of configurations over a given class
    public static Double getPrecisionSa(List<QueryGraph> qs, String focusClass, boolean exact, Set<Configuration> zs) {
        Double sumWeights = 0.0;
        Double sumWeightedPrecision = 0.0;

        for (QueryGraph qOriginal : qs) {
            double weight = qOriginal.getWeight();

            List<Double> precisions = new ArrayList<Double>();
            for (QueryVariable rootVariable : qOriginal.vertexSet()) {
                if (rootVariable.getType().equals(focusClass)) {
                    String rootVariableLabel = rootVariable.getLabel();
                    qOriginal.setRoot(rootVariable);

                    // Only outgoing edges.
                    for (PropertyEdge outgoingEdge : qOriginal.outgoingEdgesOf(rootVariable)){
                        boolean isDirOut = true;
                        QueryVariable targetVar = qOriginal.getEdgeTarget(outgoingEdge);
                        String targetVariableLabel = targetVar.getLabel();
                        String targetClass = targetVar.getType();
                        String edgeLabel = outgoingEdge.getLabel();
                        boolean isOp = false;
                        if (isObjectVariable(targetVar, navigationGraph)) { isOp = true; }

                        // For now we only consider data props
                        if (!isOp) {
                            RootedQueryGraph qCopy = new RootedQueryGraph(qOriginal);
                            QueryVariable rootVariableCopy = qCopy.getVariable(rootVariableLabel);
                            QueryVariable targetVariableCopy = qCopy.getVariable(targetVariableLabel);
                            qCopy.removeVertex(targetVariableCopy);

                            Double prec = getPrecisionSa(qCopy, edgeLabel, targetClass, exact, zs);
                            if (!(prec == null)) precisions.add(prec);
                        }
                    }
                }
            }
            System.out.println("calculated precision, here is the list of precisions for the query:");
            System.out.println(precisions);

            // Calculate the average precision for query
            if (precisions.size() > 0) {
                double averagePrecisonForQuery = 0.0;
                for (double p : precisions) averagePrecisonForQuery += p;
                averagePrecisonForQuery = (double)averagePrecisonForQuery/(double)precisions.size();

                // update values over all queries
                sumWeights += weight;
                sumWeightedPrecision += averagePrecisonForQuery*weight;
            }
        }

        // Collect everything into final weighted average precision
        return (double)sumWeightedPrecision/(double)sumWeights;
    }

    // here we consider all classes at the same time
    // for each query in the list of queries, pick all as root, and pick all outgoing as edge
    // for each query, get average precision of all outgoing class and property pairs. Then do weighted average over all queries.
    // I.e. it takes as imput a full set of configuraiotns, or a configuration forest if you like to call it that.
    public static Double getPrecisionSa(List<QueryGraph> qs, Map<String, Set<Configuration>> zMap, boolean exact) {
        Double sumWeights = 0.0;
        Double sumWeightedPrecision = 0.0;

        for (QueryGraph qOriginal : qs) {
            double weight = qOriginal.getWeight();

            List<Double> precisions = new ArrayList<Double>();
            for (QueryVariable rootVariable : qOriginal.vertexSet()) {
                if (isObjectVariable(rootVariable, navigationGraph)) {
                    String rootVariableLabel = rootVariable.getLabel();
                    String rootVariableClass = rootVariable.getType();
                    qOriginal.setRoot(rootVariable);

                    // loop over all outgoing properties
                    for (PropertyEdge outgoingEdge : qOriginal.outgoingEdgesOf(rootVariable)){
                        QueryVariable targetVar = qOriginal.getEdgeTarget(outgoingEdge);
                        String targetVariableLabel = targetVar.getLabel();
                        String edgeLabel = outgoingEdge.getLabel();

                        // only consider if data prop
                        if (!(isObjectVariable(targetVar, navigationGraph))) {
                            RootedQueryGraph qCopy = new RootedQueryGraph(qOriginal);
                            QueryVariable rootVariableCopy = qCopy.getVariable(rootVariable.getLabel());
                            QueryVariable targetVariableCopy = qCopy.getVariable(targetVar.getLabel());
                            qCopy.removeVertex(targetVariableCopy);
                            Set<Configuration> configForestForClass = zMap.getOrDefault(rootVariableClass, new HashSet<Configuration>());
                            Double prec = getPrecisionSa(qCopy, edgeLabel, targetVar.getType(), exact, configForestForClass);
                            if (!(prec == null)) precisions.add(Math.min(prec, 1.0));
                        }
                    }
                }
            }

            // Calculate the average precision for query
            if (precisions.size() > 0) {
                double averagePrecisonForQuery = 0.0;
                for (double p : precisions) {
                    averagePrecisonForQuery += p;
                }
                averagePrecisonForQuery = (double)averagePrecisonForQuery/(double)precisions.size();

                // store categorize them by precision
                if (averagePrecisonForQuery > 0.60 && averagePrecisonForQuery < 0.64) {
                    q062.add(qOriginal);
                }
                else if (averagePrecisonForQuery > 0.49 && averagePrecisonForQuery < 0.52) {
                    q05.add(qOriginal);
                }
                else if (averagePrecisonForQuery > 0.43 && averagePrecisonForQuery < 0.45) {
                    q044.add(qOriginal);
                }
                else {
                    qOther.add(qOriginal);
                }

                // update values over all queries
                sumWeights += weight;

                sumWeightedPrecision += averagePrecisonForQuery*weight;
            }
        }

        // Collect everything into final weighted average precision
        return (double)sumWeightedPrecision/(double)sumWeights;
    }















    //////////////////////////////////////////////////////
    // Precisions using one query (and one config) below
    //////////////////////////////////////////////////////


    // The precision of So = So/So, which is always 1. Divide by itself give always 1
    public static Double getPrecisionSoOneQuery(RootedQueryGraph q, String propertyLabel, String extensionClass, boolean exact) {
        return 1.0;
    }

    // The precision of Sr is So/Sr
    public static Double getPrecisionSr(RootedQueryGraph q, String propertyLabel, String extensionClass, boolean exact) {
        Double cSr = countSr(q, propertyLabel, extensionClass, exact);
        Double cSo = countSo(q, propertyLabel, extensionClass, exact);
        if (cSo == null) return null;
        if (cSr == null) return null;
        if (cSr == 0)  return 1.0;
        return (double)cSo/(double)cSr;
    }


    // The precision of Sa, one config
    public static Double getPrecisionSa(QueryGraph q, String propertyLabel, String extensionClass, boolean exact, Configuration z) {
        long a1 = System.nanoTime();
        RootedQueryGraph rq = new RootedQueryGraph(q);

        // Check if the pextension pair is in the config, if not return 0.0
        boolean extensionInConfig = doesConfigContainExtensionPair(z, propertyLabel, extensionClass);
        long a2 = System.nanoTime();
        if (!(extensionInConfig)) return 0.0;  // Extension was not in config. Then the correct precision is 0.

        // Calculate the needed counts
        Double cSa = countSa(rq, propertyLabel, extensionClass, exact, z); 
        long a3 = System.nanoTime();
        Double cSo = countSo(rq, propertyLabel, extensionClass, exact); 
        long a4 = System.nanoTime();

        if (cSo == null) return null;
        if (cSa == null) return null;
        if (cSa == 0)  return 1.0;
        return (double)cSo/(double)cSa;
    }














    ////////////////////////////////////////////////////////////
    // Calculating exact or estimated counts for Sa, So and Sr
    // Given a single query, and an extension pair, and possibly a configuration
    ////////////////////////////////////////////////////////////

    // Approximated count
    // Here we assume that the extension exists in the config. This check must be done before every time this is used.
    public static Double countSa(RootedQueryGraph q, String propertyLabel, String extensionClass, Boolean exact, RootedQueryGraph z) {
        RootedQueryGraph qCopy = new RootedQueryGraph(q);  // Copy the query to not modify the original

        // When extension is in config, pruneSimple and run normal variation.
        RootedQueryGraph qPruned = pruneSimple(qCopy, z);

        // Extend the pruned query with the relevant variable
        QueryVariable exVar = new QueryVariable("exVar", extensionClass);
        qPruned.addVertex(exVar);
        qPruned.addEdge(qPruned.getRoot(), exVar, new PropertyEdge(propertyLabel)); 
        return getAnsV(qPruned, exVar, exact);
    }



    // Optimal count
    // q is the partial query, we want to extend it and count distinct variables for the extension variable
    public static Double countSo(RootedQueryGraph q, String propertyLabel, String extensionClass, Boolean exact) {
        RootedQueryGraph qCopy = new RootedQueryGraph(q);  // Copy the query to not modify the original

        // Extend the query with the relevant variable, then return the distinct answers of the query over this variable.
        QueryVariable exVar = new QueryVariable("exVar", extensionClass);
        qCopy.addVertex(exVar);
        qCopy.addEdge(qCopy.getRoot(), exVar, new PropertyEdge(propertyLabel)); 
        return getAnsV(qCopy, exVar, exact);
    }

    // Get the exact number of distinct values suggested by Sr
    // This count is in fact one of the stats we already have. 
    // Notice that exact and estimated is the same
    public static double countSr(RootedQueryGraph q, String propertyLabel, String extensionClass, Boolean exact) {
        String focusClass = q.getRoot().getType();
        return getStatDT(focusClass, propertyLabel, extensionClass);
    }




























    ///////////////////////////////////////////////////////////
    // Wrapper functions using exact or estimate as parameter
    // set exact == true if you want exact answers, false otherwise
    ///////////////////////////////////////////////////////////

    public static Double getAns(RootedQueryGraph q, boolean exact) {
        q.redirectEdges();
        if (exact) {
            Integer r = exactAns(q);
            if (r == null) return null;
            return Double.valueOf(r);
        }
        return estimateAns(q);
    }

    public static Double getAnsV(RootedQueryGraph q, QueryVariable v, boolean exact) {
        q.redirectEdges();
        if (exact) {
            Integer r = exactAnsV(q,v);
            if (r == null) return null;
            return Double.valueOf(r);
        }
        return estimateAnsV(q, v);
    }

    public static Double getAnsOpt(RootedQueryGraph q, boolean exact) {
        q.redirectEdges();
        if (exact) {
            Integer r = exactAnsOpt(q);
            if (r == null) return null;
            return Double.valueOf(r);
        }
        return estimateAnsOpt(q);
    }

    public static Double getAnsOptEx(RootedQueryGraph q, boolean exact) {
        q.redirectEdges();
        if (exact) {
            Integer r = exactAnsOptEx(q);
            if (r == null) return null;
            return Double.valueOf(r);
        }
        return estimateAnsOptEx(q);
    }










    /////////////////////////////////////////////////////////////////
    // EXACT CARDINALITIES FOR Ans, AnsV, AnsOpt, AnsoptEx
    /////////////////////////////////////////////////////////////////



    // Get the exact number of results of a standard query
    public static Integer exactAns(RootedQueryGraph q) {
        String queryString = "SELECT (COUNT(*) AS ?count) WHERE {\n";
        queryString += q.toPatternBasic();
        queryString += "}";
        System.out.println(queryString);
        return getCountFromQuery(queryString);
    }

    // Give a query and a variable in this query. Return the number of distinct values this variable takes.
    // Same as AnsV
    public static Integer exactAnsV(RootedQueryGraph q, QueryVariable v) {
        String queryString = "SELECT (COUNT(DISTINCT ?" + v.getLabel()  + ") AS ?count) WHERE {\n";
        queryString += q.toPatternBasic();
        queryString += "}";
        Integer us = getCountFromQuery(queryString);
        return us;
    }

    // Give a query, and get the exact optional cardinality
    public static Integer exactAnsOpt(RootedQueryGraph q) {
        String queryString = "SELECT (COUNT(*) AS ?count) WHERE {\n";
        queryString += q.toPatternOptional();
        queryString += "}";
        System.out.println(queryString);
        return getCountFromQuery(queryString);
    }


    // Give a query, and get the exact optional with extensionals cardinality
    // Exact AnsOptEx
    // this one does not remove subfunctions, it only collapses the same tuples after bound.
    public static Integer exactAnsOptEx(RootedQueryGraph q) {
        System.out.println(q);
        String queryString = "SELECT (COUNT(*) AS ?count) WHERE {\n";
        queryString += "SELECT DISTINCT ";

        System.out.println("flag11");
        for (QueryVariable v : q.vertexSet()) {
            if (isObjectVariable(v, navigationGraph)) { queryString += "(BOUND(?" + v.getLabel() + ") AS ?bound_" + v.getLabel() + ") "; }
            else { queryString += "?" + v.getLabel() + " "; }
        }
        queryString += "WHERE {\n";
        queryString += q.toPatternOptional();
        queryString += "}\n";
        queryString += "}";
        System.out.println("ansopex");
        System.out.println(queryString);
        return getCountFromQuery(queryString);
    }



















    ///////////////////////////////////////////////////////////////////////////////
    // BELOW: ESTIMATE Ans, AnsOpt, AnsOptEx and AnsV
    ///////////////////////////////////////////////////////////////////////////////


    public static Double estimateAns(RootedQueryGraph q) {
        Double p = Double.valueOf(getStatCF(q.getRoot().getType()));
        for (PropertyEdge pe : q.edgeSet()) {
            double bfCurrent = bf(q.getEdgeSource(pe).getType(), pe.getLabel(), q.getEdgeTarget(pe).getType()); 
            p*= bfCurrent;
        }
        return p;
    }



    // recursive helper function for martins formula for ansopt.gg
    public static Double calculateCRec(RootedQueryGraph q, QueryVariable v) {
        Double prod = 1.0;

        for (PropertyEdge edge : q.outgoingEdgesOf(v) ){
            QueryVariable w = q.getEdgeTarget(edge);

            String sourceClass = v.getType();
            String targetClass = w.getType();

            Double nf = 1.0 - ((double)getStatDS(sourceClass, edge.getLabel(), targetClass)/(double)getStatCF(sourceClass));
            Double bf = (double)getStatPF(sourceClass, edge.getLabel(), targetClass)/(double)getStatCF(sourceClass);

            prod *= (nf + bf * calculateCRec(q, w));
        }
        return prod;

    }


    // using branching factor with null values considered.
    public static Double estimateAnsOpt(RootedQueryGraph q) {
        Double p = Double.valueOf(getStatCF(q.getRoot().getType())); // number of instances of class
        Double c = calculateCRec(q, q.getRoot());
        return p*c;
    }



    public static Double estimateAnsOptOld(RootedQueryGraph q) {
        long a = System.nanoTime();
        Double p = Double.valueOf(getStatCF(q.getRoot().getType()));
        long b = System.nanoTime();
        for (PropertyEdge pe : q.edgeSet()) {
            Double kk = bfOpt(q.getEdgeSource(pe).getType(), pe.getLabel(), q.getEdgeTarget(pe).getType()); 
            QueryVariable targetClass = q.getEdgeTarget(pe);
            String t = targetClass.getType();
            p *= kk;
        }
        long c = System.nanoTime();
        return p;
    }



    // Distinct single variable cardinality
    // Here we must add the 
    // This is the Ans_v(Q,D) thing
    // We look at all instances with same type as v. The number of these is the upper limit on what to return.
    // We know that v is connected to the root via some variable, so we can use this to limit more. I.e. removing instances without the connection.
    // Now we also know how many tuples satiesfies all variables in Q, this is Ans(Q,D). If we assume that they hit a random of the instances mentioned above, we can calculate an estimated value of how many they hit.
    // Here v must be connected to the root of Q - This is used to calculate how many of the instances have the connection to the root.
    public static Double estimateAnsV(RootedQueryGraph q, QueryVariable v){
        double ansEstimate = estimateAns(q);
        QueryVariable root = q.getRoot();

        for (PropertyEdge pe : q.outgoingEdgesOf(root)) {
            QueryVariable target = q.getEdgeTarget(pe);
            if (target.equals(v)) {
                int distinctValuesForV = getStatDT(q.getEdgeSource(pe).getType(), pe.getLabel(), q.getEdgeTarget(pe).getType());
                return ballsToBoxes(ansEstimate, Double.valueOf(distinctValuesForV));
            }
        }
        return null;
    }


    // ansoptEx new version growing
    // It totally overestimates. It assumes that every row is multiplied by the set of distinct values, which is not at all true.
    public static Double estimateAnsOptExOld2(RootedQueryGraph q) {

        Double returnProd = 1.0;
        for (PropertyEdge pe : q.edgeSet()) {
            QueryVariable sourceVar = q.getEdgeSource(pe);
            QueryVariable targetVar = q.getEdgeTarget(pe);
            if (isObjectVariable(targetVar, navigationGraph)) {
                returnProd *= 1.0;
            }
            else {
                double kk = getStatDT(sourceVar.getType(), pe.getLabel(), targetVar.getType() );
                if (kk == 0.0) { kk = 1.0;  }
                returnProd *= kk;
            }
        }
        return returnProd;
    }

    public static Double getNewEstimatePossibleTuples(RootedQueryGraph q) {
        Double number = 1.0;
        // go over all data edges and count possible
        for (PropertyEdge pe : q.edgeSet()) {
            QueryVariable targetVar = q.getEdgeTarget(pe);
            QueryVariable sourceVar = q.getEdgeSource(pe);
            String sourceType = sourceVar.getType();
            String targetType = targetVar.getType();

            if (!isObjectVariable(targetVar, navigationGraph)) {
                number *= ((double)getStatDT(sourceType, pe.getLabel(), targetType) + 1); // +1 for null. Make sure that result is not 0 when a data prop is not in data.
            }
        }

        return number;
    }

    public static Double estimateAnsOptEx(RootedQueryGraph q) {
        Map<QueryVariable, Set<QueryVariable>> childrenMap = q.calculateChildrenMap();
        Map<QueryVariable, QueryVariable> parentMap = q.calculateParentMap();

        Double ansOptEstimate = getAnsOpt(q, false);
        double possibleTuplesEstimate =  getNumberOfTuplesOfSubtree(q.getRoot(), q, childrenMap, parentMap, ansOptEstimate);
        double possibleTuplesEstimateNew = getNewEstimatePossibleTuples(q); // using martins only data prop method
        if (possibleTuplesEstimate < 0 || ansOptEstimate < 0) {
            try{System.in.read();}
            catch(Exception e) {}
        }
        double result = ballsToBoxes(ansOptEstimate, possibleTuplesEstimate);
        return result;
    }



    // if data var: return distinct vars in dataset of target of the edge leading to variable.
    // if object prop: 
    // Used as a part of the ansOptEx to find out how many distinct values a tuple can take.
    public static double getNumberOfTuplesOfSubtree(QueryVariable v, RootedQueryGraph q, Map<QueryVariable, Set<QueryVariable>> childrenMap, Map<QueryVariable, QueryVariable> parentMap, double ansOptEstimate) {

        if (isClass(v.getType())) {
            double childrenProd = 1.0;
            for (QueryVariable child : childrenMap.getOrDefault(v, new HashSet<QueryVariable>())) {
                double ss = getNumberOfTuplesOfSubtree(child, q, childrenMap, parentMap, ansOptEstimate);
                childrenProd *= ss;
                if (childrenProd > Double.valueOf("2.0E250")) {
                    try{
                        System.out.println("number over 1e250. Remember that 1e308 is the largest possible.");
                        System.in.read();
                    }
                    catch (Exception e){}
                }
            }

            if (v.equals(q.getRoot())) {
                return childrenProd; // If it is the root, then we have to return the product of each children.
            }
            else {
                double nn = childrenProd + 1.0; // I think this is plus one for non exists
                return nn;
            }
        }
        // When the variable is typed to datatype
        else {
            QueryVariable parentVar = parentMap.get(v); // get the parent

            // just get the edge from its parent to the variable. Every data variable has a parent.
            PropertyEdge edge = null;
            Set<PropertyEdge> eForward = q.getAllEdges(parentVar, v); // get all edges from the parent to this variable
            for (PropertyEdge e : eForward) { edge = e; } 

            int uu = getStatDT(q.getEdgeSource(edge).getType(), edge.getLabel(), q.getEdgeTarget(edge).getType()) + 1;  // add one because it can be null

            return Double.valueOf(uu);
        }
    }





    // Calculate the expected number of boxes covereed when there are n boxes and k balls.
    // this version of balls to boxes assumes a uniform distribution
    public static Double ballsToBoxes(Double k, Double n) {

        double inf = Double.POSITIVE_INFINITY;


        if (k.equals(0)) {
            return 0.0;
        }

        if (k.equals(inf)) {
            System.out.println("k is infinity");
        }
        if (n.equals(inf)) {
            System.out.println("n is infinity");
        }

        // When n is very small, then we have problems calculating values for the 
        if (k > n*Double.valueOf("1.0e6")) { 
            return n; 
        }  // k is way larger, then return n
        if (n > k*Double.valueOf("1.0e6")) { 
            return k; 
        }  // n is way larger, then return k

        if (k > Double.valueOf("1.0e8") && n > Double.valueOf("1.0e8") ) { 
            System.out.println("warning: used e-formula from Jorgen."); 

            return n*(1-Math.pow(Math.E, -(k/n)));
        }  
        return n - n * Math.pow(((double)1.0-(double)1.0/(double)n), k); // The actual formula. Used when k and n are small
    }




    // Calculate the expected number of boxes covereed when there are n boxes and k balls.
    // thie version assums a longtail distribution, I used zipfs distribution
    public static Double ballsToBoxesZipf(Double k, Double n) {

        double s = 2.5; // zipfs formula s. If s is large, then almost every value is the same, if it is low, then it is uniforml

        double c = 0;
        for (int i = 1 ; i <= n ; i++) {
            c += Math.pow((double)1.0/(double)i, s);
        }

        double c2 = 1.0/(Math.pow(n , s-1));
        System.out.println("c = " + c);
        System.out.println("c2 = " + c2);


        if (n > 100.0) n = 100.0;
        double sum = 0;
        for (int i = 1 ; i <= n ; i++) {
            sum += (1 - Math.pow( (1.0-     1.0/((  Math.pow(i,s) )* c )      ), k));
        }

        return (double)(sum);
    }































    /////////////////////////////////////////////////////////////////////////////////////
    /// BRANCING FACTORS for basic and opt. Can be calculated directy based on the stats
    /////////////////////////////////////////////////////////////////////////////////////

    // Branching factor basic
    // So if we have an instance of type source, and we expand to target, how much will it branch?
    public static double bf(String sourceClass, String property, String targetClass) {
        return (double)getStatPF(sourceClass, property, targetClass)/(double)getStatCF(sourceClass);
    }


    // Branching factor optional
    public static double bfOpt(String sourceClass, String property, String targetClass) {
        return 1 + (double)(getStatPF(sourceClass, property, targetClass) - getStatDS(sourceClass, property, targetClass) )/(double)getStatCF(sourceClass);
    }




    ////////////////////////////////////////////////////////////////////////////////////////
    // CACHED RESULTS FOR Nav Graph - Statistics used to estimate cardinality
    ////////////////////////////////////////////////////////////////////////////////////////


    // Given the label of a class, count gets the exact number of instances of this class in dataset.
    // I.e. get the number of distinct individuals in dataset of class classLabel
    // This method just gets it as fast as possible.
    public static int getStatCF(String classLabel) {
        String filePath = "./files/cache/CF.tsv";
        if (mapCF == null) mapCF = Utils.getMapFromFile(filePath);
        if (!mapCF.containsKey(classLabel)) {
            String queryString = "SELECT (COUNT(*) AS ?count) WHERE {\n";
            queryString += "?i <" + Utils.getRdfTypeString() + "> <" + classLabel + ">.\n";
            queryString += "}";
            int cardinality = getCountFromQuery(queryString);
            mapCF.put(classLabel, cardinality);
            Utils.writeMapToFile(mapCF, filePath);
        }
        return mapCF.get(classLabel);
    }


    // PF: propety frequency
    // Given an edge in the navigation graph, represented by a source uri, the property uri and the target uri. 
    // Return the number of typed triples in dataset. This is either fetched from cache, or retrieved from dataset.
    public static int getStatPF(String sourceUri, String propertyUri, String targetUri) {

        String filePath = "./files/cache/PF.tsv";
        String key = sourceUri + " " + propertyUri + " " + targetUri;
        if (mapPF == null) mapPF = Utils.getMapFromFile(filePath);
        if (!mapPF.containsKey(key)) {

            String queryString = "SELECT (COUNT(*) AS ?count) WHERE {\n";
            if (propertyUri.endsWith("^")) queryString += "?target " + "<" + Utils.getInverseProperty(propertyUri) + ">" + " ?source.\n";
            else queryString += "?source " + "<" + propertyUri + ">" + " ?target.\n";
            if (isClass(sourceUri)) { queryString += "?source <" + Utils.getRdfTypeString() + "> <" + sourceUri + ">.\n"; }
            if (isClass(targetUri)) { queryString += "?target <" + Utils.getRdfTypeString() + "> <" + targetUri + ">.\n"; }
            queryString += "}";

            System.out.println(queryString);
            int cardinality = getCountFromQuery(queryString);
            mapPF.put(key, cardinality);
            Utils.writeMapToFile(mapPF, filePath);
        }
        return mapPF.get(key);
    }


    // distinct data sources
    public static int getStatDS(String sourceClass, String property, String targetClass) {
        String filePath = "./files/cache/DS.tsv";
        String key = sourceClass + " " + property + " " + targetClass;
        if (mapDS == null) mapDS = Utils.getMapFromFile(filePath);
        if (!mapDS.containsKey(key)) {
            String queryString = "";
            queryString += "SELECT (COUNT(*) AS ?count) WHERE {\n";
            queryString += "SELECT DISTINCT ?source WHERE {\n";

            if (property.endsWith("^")) queryString += "?target " + "<" + Utils.getInverseProperty(property) + ">" + " ?source.\n";
            else queryString += "?source " + "<" + property + ">" + " ?target.\n";

            if (isClass(sourceClass)) { queryString += "?source <" + Utils.getRdfTypeString() + "> <" + sourceClass + ">.\n"; }
            if (isClass(targetClass)) { queryString += "?target <" + Utils.getRdfTypeString() + "> <" + targetClass + ">.\n"; }
            queryString += "}\n";
            queryString += "}";
            int cardinality = getCountFromQuery(queryString);
            mapDS.put(key, cardinality);
            Utils.writeMapToFile(mapDS, filePath);
        }
        return mapDS.get(key);
    }

    // STatistics about a property between two types, either class or datatypes
    // If it is stored in file, use that count.
    // otherwise, ask  dataset
    public static int getStatDT(String sourceClass, String property, String targetClass) {
        String filePath = "./files/cache/DT.tsv";
        String key = sourceClass + " " + property + " " + targetClass;
        if (mapDT == null) mapDT = Utils.getMapFromFile(filePath);
        if (!mapDT.containsKey(key)) {
            String queryString = "";
            queryString += "SELECT (COUNT(*) AS ?count) WHERE {\n";
            queryString += "SELECT DISTINCT ?target WHERE {\n";

            if (property.endsWith("^")) queryString += "?target " + "<" + Utils.getInverseProperty(property) + ">" + " ?source.\n";
            else queryString += "?source " + "<" + property + ">" + " ?target.\n";

            if (isClass(sourceClass)) { queryString += "?source <" + Utils.getRdfTypeString() + "> <" + sourceClass + ">.\n"; }
            if (isClass(targetClass)) { queryString += "?target <" + Utils.getRdfTypeString() + "> <" + targetClass + ">.\n"; }
            queryString += "}\n";
            queryString += "}";
            int cardinality = getCountFromQuery(queryString);
            mapDT.put(key, cardinality);
            Utils.writeMapToFile(mapDT, filePath);
        }
        return mapDT.get(key);
    }


    //////////////////////////////////////
    //// Count Query over dataset, with caching
    //////////////////////////////////////

    // In many cases we are just interested in a count
    // This will also be cached. I.e we check if the query has been asked before, and if so we just fetch it from the queries.tsv file.
    public static Integer getCountFromQuery(String queryString) {
        queryString = queryString.replace("\n", " ").replace("\r", " "); 
        String filePath = "./files/cache/queries.tsv";
        Map <String, Integer> map = Utils.getMapFromFile(filePath);
        if (!map.containsKey(queryString)) {
            ResultSet rs = d.runQuery(queryString);
            if (rs == null) return null;
            Integer cardinality =  Utils.getFirstIntRowByVariableName(rs, "count");
            map.put(queryString, cardinality);
            Utils.writeMapToFile(map, filePath);
        }
        else System.out.println("Found this query in file! Saving time by using cache. Query: " + queryString.replace("\n", " "));
        return map.get(queryString);
    }



    ////////////////////////////////////////////////////////
    // Just pruning function
    ////////////////////////////////////////////////////////

    // return a query which is a subgraph of q1, but with corresponding variables in q2. I.e it is like an intersection of q1 and q2
    // Here we assume that there is only one of each kind of the properties, i.e simple configs. This ensures that there is only one way to prune
    // Use a queue to pruneSimple query.
    // Start with the root, make a queue, visit all nodes in the query.
    // If we find corresponding nodes in q2, then we keep it and continue to expand.
    public static RootedQueryGraph pruneSimple(RootedQueryGraph q1, RootedQueryGraph q2) {
        q1.redirectEdges();
        q2.redirectEdges();

        // Get the childrenMaps
        Map<QueryVariable, Set<QueryVariable>> q1ChildrenMap = q1.calculateChildrenMap();
        Map<QueryVariable, Set<QueryVariable>> q2ChildrenMap = q2.calculateChildrenMap();
        Map<QueryVariable, QueryVariable> q1ParentMap = q1.calculateParentMap();
        Map<QueryVariable, QueryVariable> q2ParentMap = q2.calculateParentMap();

        // The queue to use
        Map <QueryVariable, QueryVariable> map1to2 = new HashMap<QueryVariable, QueryVariable>();
        if (!q1.getRoot().getType().equals(q2.getRoot().getType())) {
            System.out.println("Not same root type");
            return null;
        }

        map1to2.put(q1.getRoot(), q2.getRoot());
        Set <QueryVariable> used1 = new HashSet<QueryVariable>();
        Set <QueryVariable> used2 = new HashSet<QueryVariable>();
        used1.add(q1.getRoot());
        used2.add(q2.getRoot());

        // Everything added to the queue must have mapping
        Queue<QueryVariable> q = new LinkedList<QueryVariable>();
        q.add(q1.getRoot());

        while (q.size() > 0) {
            QueryVariable currentVar1 = q.remove();
            QueryVariable currentVar2 = map1to2.get(currentVar1);
            for (PropertyEdge pe1 : q1.outgoingEdgesOf(currentVar1)) {
                QueryVariable targetVar1 = q1.getEdgeTarget(pe1);
                for (PropertyEdge pe2 : q2.outgoingEdgesOf(currentVar2)) {
                    QueryVariable targetVar2 = q2.getEdgeTarget(pe2);

                    if (q1ChildrenMap.getOrDefault(currentVar1, new HashSet<QueryVariable>()).contains(targetVar1)) {
                        if (q2ChildrenMap.get(currentVar2).contains(targetVar2)) {
                            if (pe1.getLabel().equals(pe2.getLabel())) {
                                if (targetVar1.getType().equals(targetVar2.getType())) {
                                    if (!used1.contains(targetVar1)) {
                                        if (!used2.contains(targetVar2)) {
                                            map1to2.put(targetVar1, targetVar2);
                                            used1.add(targetVar1);
                                            used2.add(targetVar2);
                                            q.add(targetVar1);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        // Make a completely new query from this mapping
        RootedQueryGraph out = new RootedQueryGraph(q1.getNavigationGraph());
        for (QueryVariable v : q1.vertexSet()) {
            if (used1.contains(v)) out.addVertex(v);
        }
        for (PropertyEdge pe : q1.edgeSet()) {
            if (used1.contains(q1.getEdgeSource(pe))) {
                if (used1.contains(q1.getEdgeTarget(pe))) {
                    out.addEdge(q1.getEdgeSource(pe), q1.getEdgeTarget(pe), pe);
                }
            }
        }
        out.setRoot(q1.getRoot());

        return out;
    }

}