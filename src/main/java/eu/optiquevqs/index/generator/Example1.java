package eu.optiquevqs.index.generator;

import eu.optiquevqs.graph.*;
import eu.optiquevqs.graph.navigation.*;

public class Example1 {

    public static void main(String[] args) throws Exception {

        // How to make a navigation graph
        NavigationGraph ng = new NavigationGraph();
        ClassNode personClass = new ClassNode("Person");
        DatatypeNode stringDatatype = new DatatypeNode("String");
        ng.addVertex(personClass);
        ng.addVertex(stringDatatype);
        ng.addEdge(personClass, stringDatatype, new PropertyEdge("name"));
        System.out.println(ng);


    }
}
