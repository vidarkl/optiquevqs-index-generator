package eu.optiquevqs.index.generator.core;

import org.apache.jena.query.*;

// A connection to a dataset (in blazegraph)
public class GraphDatasetConnection {

    // dataset uri
    private String datasetURI;

    // Constructor
    public GraphDatasetConnection(String datasetURI) {
        this.datasetURI = datasetURI;
    }




    // Query over an endpoint and return results
    public ResultSet runQuery(String sparqlQueryString) {
        try {
            Query query = QueryFactory.create(sparqlQueryString);
            QueryExecution qexec = QueryExecutionFactory.sparqlService(datasetURI, query);
            qexec.setTimeout(300000); // 5 minutes
            ResultSet results = qexec.execSelect();
            return results;
        }
        catch (Exception e){
            System.out.println("[ERROR] Failed to query over triple store." + e);
            return null;
        }
        finally {
            // qexec.close();
        }
    }









}

