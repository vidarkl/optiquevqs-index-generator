package eu.optiquevqs.index.generator.core;


import eu.optiquevqs.graph.query.QueryGraph;
import eu.optiquevqs.graph.query.*;
import eu.optiquevqs.graph.navigation.NavigationGraph;
import eu.optiquevqs.index.generator.*;
import eu.optiquevqs.graph.*;


import java.util.*;
import java.util.Queue; 



// This Class is an extension of the normal query, where a parent function is given.
// It also contains methods which are not useful in other contexts than this index.
// We will also use a rooted query to represent configurations
// It will also only have edges going out from root.
public class RootedQueryGraph extends QueryGraph {

    // Empty constructor
    public RootedQueryGraph() {
        super();
    }

    // Constructor, make a new rooted query from ng
    public RootedQueryGraph(NavigationGraph ng) {
        super(ng);
    }

    // constructor. Turn all edges correct way
    public RootedQueryGraph (QueryGraph another) {
        super(another);
    }

    // Change all edges such that they points away from the root. If needed use ^ in end of the property uri.
    public void redirectEdges() {
        RootedQueryGraph copy = new RootedQueryGraph(this);

        // calculate parent map before all edges are removed
        Map <QueryVariable, QueryVariable> parentMap = this.calculateParentMap();

        // remove all edges
        List <PropertyEdge> edges = new ArrayList<PropertyEdge>();
        for (PropertyEdge e : this.edgeSet()) edges.add(e);
        this.removeAllEdges(edges);

        for (Map.Entry<QueryVariable, QueryVariable> entry : parentMap.entrySet()) {
            QueryVariable child = entry.getKey();
            QueryVariable parent = entry.getValue();

            if (parent != null) {
                for (PropertyEdge pe :copy.getAllEdges(parent, child)) {
                    this.addEdge(parent, child, new PropertyEdge(pe.getLabel()));
                }
                for (PropertyEdge pe : copy.getAllEdges(child, parent)) {
                    this.addEdge(parent, child, new PropertyEdge(Utils.getInverseProperty(pe.getLabel())));
                }
            }
        }
    }


    // Get a variable name that is not used yet.
    public String getFreeVariableName() {
        int i = 0;
        while (true) {
            i+=1; 
            boolean foundVar = false;
            for (QueryVariable va : this.vertexSet()) {
                if (va.getLabel().equals("v" + Integer.toString(i) )) {
                    foundVar = true;
                }
            }
            if (!foundVar) {
                return "v" + Integer.toString(i); 
            }
        }
    }


    // Get the corresponding SPARQL pattern of a rooted query.
    public String toPatternBasic() {
        String patternString = "";
        for (QueryVariable variable : this.vertexSet()) {
            if (this.isObjectVariable(variable)) {
                patternString += "?" + variable.getLabel() + " <" + Utils.getRdfTypeString() + "> " + "<" +variable.getType() + ">" + ".\n";
            }
        }

        for (PropertyEdge edge : this.edgeSet()) {
            String propertyUri = edge.getLabel();
            if (propertyUri.endsWith("^")) patternString += "?" + this.getEdgeTarget(edge).getLabel() + " <" + Utils.getInverseProperty(propertyUri) + "> " + "?" + this.getEdgeSource(edge).getLabel() + " .\n";
            else patternString += "?" + this.getEdgeSource(edge).getLabel() + " <" + propertyUri + "> " + "?" + this.getEdgeTarget(edge).getLabel() + " .\n";
        }
        return patternString;
    }








    // Get the SPARQL pattern with optionals for each branch
    public String toPatternOptional() {
        // Calculate children map
        Map<QueryVariable, Set<QueryVariable>> childrenMap = this.calculateChildrenMap();
        return recursiveAddOptional(this.getRoot(), childrenMap);
    }

    // Add a optional parts recursively.
    private String recursiveAddOptional(QueryVariable qv, Map<QueryVariable, Set<QueryVariable>> childrenMap) {

        String returnString = "";
        if (this.isObjectVariable(qv)) {
            returnString += "?" + qv.getLabel() + " " + "<" + Utils.getRdfTypeString() + ">" +  " " + "<" + qv.getType() + ">" + ".\n";
        }

        Set<QueryVariable> children = childrenMap.getOrDefault(qv, new HashSet<QueryVariable>());
        for (PropertyEdge pe : this.outgoingEdgesOf(qv)) {
            String propertyUri = pe.getLabel();
            QueryVariable targetVariable = this.getEdgeTarget(pe);
            if (children.contains(targetVariable)) {
                returnString += "OPTIONAL {";

                if (propertyUri.endsWith("^")) returnString += "?" + targetVariable.getLabel() + " <" + Utils.getInverseProperty(propertyUri) + "> " + "?" + qv.getLabel() + " .\n";
                else returnString += "?" + qv.getLabel() + " <" + propertyUri + "> " + "?" + targetVariable.getLabel() + " .\n";

                returnString += recursiveAddOptional(targetVariable, childrenMap);
                returnString += "}\n";
            }
        }
        return returnString;
    }



    // Calculate the parent function, returning a map from every vertex to its parent.
    public Map<QueryVariable, QueryVariable> calculateParentMap() {
        Map<QueryVariable, QueryVariable> parentMap = new HashMap<QueryVariable, QueryVariable>();
        QueryVariable rootVariable = this.getRoot();

        parentMap.put(rootVariable, null);
        Queue<QueryVariable> q = new LinkedList<QueryVariable>(); 
        q.add(rootVariable);

        // iterate over queue and use it as parent
        while (q.size() > 0) {
            QueryVariable parentVar = q.remove();
            Set<PropertyEdge> incomingEdges = this.incomingEdgesOf(parentVar);
            Set<PropertyEdge> outgoingEdges = this.outgoingEdgesOf(parentVar);
            Set<PropertyEdge> allEdges = new HashSet<PropertyEdge>();
            allEdges.addAll(incomingEdges); 
            allEdges.addAll(outgoingEdges); 

            for (PropertyEdge e : allEdges) {
                QueryVariable childVar = this.getEdgeSource(e);
                if (childVar.equals(parentVar)) {
                    childVar = this.getEdgeTarget(e);
                }

                // If the potential child is not already the parent of the parentVar, then it is a proper child.
                if (!(childVar.equals(parentMap.get(parentVar)))) {
                    parentMap.put(childVar, parentVar);
                    q.add(childVar);
                }
            }
        }
        return parentMap;
    }



    // Given a rootedquerygraph, we can calculate the children
    // If a vertex is not a key in the children map, then it has no children.
    public Map<QueryVariable, Set<QueryVariable>> calculateChildrenMap() {
        Map<QueryVariable, QueryVariable> parentMap = this.calculateParentMap();
        Map<QueryVariable, Set<QueryVariable>> childrenMap = new HashMap<QueryVariable, Set<QueryVariable>>();
        for (Map.Entry<QueryVariable, QueryVariable> entry : parentMap.entrySet()) {
            QueryVariable child = entry.getKey();
            QueryVariable parent = entry.getValue();
            if (parent != null) {
                Set<QueryVariable> currentSet = childrenMap.getOrDefault(parent, new HashSet<QueryVariable>());
                currentSet.add(child);
                childrenMap.put(parent, currentSet);
            }
        }
        return childrenMap;
    }

    // this returns a unique string representation of a simple configuration
    public String getCanonicalStringRepresentation() {
        Map<QueryVariable, Set<QueryVariable>> childrenMap = this.calculateChildrenMap();
        return getCanonicalStringRepresentationRecursive(this.getRoot(), childrenMap);
    }


    // recursive helper function for the canonical string representation
    public String getCanonicalStringRepresentationRecursive(QueryVariable v, Map<QueryVariable, Set<QueryVariable>> childrenMap ) { 
        String returnString = v.getType();
        List<String> childrenStrings = new ArrayList<String>();
        Set<QueryVariable> children = childrenMap.getOrDefault(v, new HashSet<QueryVariable>());
        returnString += "(";
        for (QueryVariable child : children) {
            // get edge name
            Set<PropertyEdge> edges = this.getAllEdges(v, child);
            PropertyEdge e = null;
            for (PropertyEdge pe : edges) e = pe;
            childrenStrings.add(e.getLabel() + " -> " + getCanonicalStringRepresentationRecursive(child, childrenMap));
        }
        // sort list of childrenStrings
        java.util.Collections.sort(childrenStrings);
        String prefix = "";
        for (String childString : childrenStrings) {
            returnString += prefix;
            prefix = ", ";
            returnString += childString;
        }
        returnString += ")";
        return returnString;
    }

}

