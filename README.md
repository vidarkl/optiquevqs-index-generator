# OptiqueVQS Index Generator

This Java project provides algorithms to search for the optimal way to make a query suggestion index for [OptiqueVQS](https://gitlab.com/ernesto.jimenez.ruiz/OptiqueVQS) (1) or other visual query systems. See (2) for a full overview of available search methods and how to apply the results to achieve desired suggestions.

This project is by default set to use data from [Wikidata Assets](https://gitlab.com/vidarkl/wikidata-assets), but the search algorithms are domain agnostic, so they can be applied to any domain.

## Instructions
The search algorithms requires access to three assets: a navigation graph, a dataset, and a query log. 


### Prepare Navigation Graph
The navigation graph specifies the structures that users are allowed to use in queries. 
Navigation graphs can be defined using the OptiqueVQS graph library ([link](https://gitlab.com/vidarkl/wikidata-assets)). 
See `src/main/java/wikidata/assets/WikidataAssets.java` in [Wikidata Assets](https://gitlab.com/vidarkl/wikidata-assets) for examples showing how to set up a navigation graph.

### Prepare Database
The software requires SPARQL access to the dataset, so we recommend adding it to an RDF database, like Blazegraph, for example.
Access to the database must be given in the beginning of the main method in `src/main/java/eu/optiquevqs/index/generator/Development.java`.

### Prepare Query Log
Queries must be placed in `files/queries`. Each query file must be a .tsv file where the first column is the weight and the second column is the SPARQL query.
Go to the main method of `src/main/java/eu/optiquevqs/index/generator/Development.java` to specify which queries to include.

### Run Generation Algorithm
The concrete search algorithm to use and which max cost threshold to use is defined in the main method of `src/main/java/eu/optiquevqs/index/generator/Development.java`. To apply the generation algorithm run (requires Maven):

```
mvn clean install
mvn exec:java@Generate
```

The generation results will be added to the main folder of the project with prefix ´results-´. Each search function generates different result files.


## References
1. Klungre, V.N., 2020. **Adaptive Query Extension Suggestions for Ontology-
Based Visual Query Systems.** Ph.D. thesis. University of Oslo, 2020, [link](https://www.duo.uio.no/handle/10852/80614)
2. Ahmet Soylu, Evgeny Kharlamov, Dimitry Zheleznyakov, Ernesto Jimenez Ruiz, Martin Giese, Martin G. Skjaeveland, Dag Hovland, Rudolf Schlatte, Sebastian Brandt, Hallstein Lie, Ian Horrocks. **OptiqueVQS: a Visual Query System over Ontologies for Industry**. Semantic Web Journal, 2018. [link](http://semantic-web-journal.net/content/optiquevqs-visual-query-system-over-ontologies-industry-0)
3. Klungre, Ahmet Soylu, Ernesto Jimenez-Ruiz, Evgeny Kharlamov, Martin Giese. **Query extension suggestions for visual query systems through ontology projection and indexing**. New Generation Computing, 2019 [link](https://link.springer.com/article/10.1007/s00354-019-00071-1)

